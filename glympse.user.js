// ==UserScript==
// @id             glympse@scholaris
// @name           IITC plugin: Glympse
// @category       Layer
// @version        1.0.0-rc
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @downluadURL
// @description    Glympse Layers for IITC
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @grant          none
// ==/UserScript==

/**
 * Known issues:
 *
 * 1. If a user is expired in one group but becomes active in a new group, location does not update.
 */

function wrapper(plugin_info) {
  // ensure plugin framework is there, even if iitc is not yet loaded
  if(typeof window.plugin !== "function") {
    window.plugin = function() {};
  }

  /**
   * Glympse User class
   *
   * Manage tracking for an individual Glympse user.
   *
   * Public functions: start, stop
   * @param {String} invite  Invite identifier
   * @param {Object} options [description]
   */
  function GlympseUser() {
    /**
     * Name to use for layer chooser
     * @type {String}
     */
    this.name = false;

    /**
     * Glympse user identifier
     * @type {String}
     */
    this.identity = false;

    /**
     * Location History
     * @type {Array}
     *
     * Ducktype:
     *   * {
     *       lat:
     *       lng:
     *     }
     */
    this.locations = [];

    /**
     * Timeout to refresh position
     * @type {Number}
     */
    this.timeout = null;

    /**
     * Leaflet Marker
     * @type {L.Marker}
     */
    this.marker = null;

    /**
     * Leaflet Polyline
     * @type {L.Polyline}
     */
    this.path = null;

    /**
     * Leaflet Feature Group
     * @type {L.FeatureGroup}
     */
    this.layer = L.featureGroup([]);

    /**
     * Invites
     * @type {Object}
     *
     * * Ducktype: invite => {
     *     next: "next"
     *     last: "last"
     *     container: GlympseGroup | GlympseLayer
     *     invite: invite
     *     options: {}
     *     status: string
     *     properties: {
     *       start_time: timestamp
     *       end_time: timestamp
     *       owner: user identifier
     *       lastUpdated: Date
     *       app: {name, icon}
     *       name: string
     *       avatar: string/url
     *       battery_mode: string ("regular", "low")
     *     }
     *   }
     */
    this.invites = {};

    this.options = false;

    this.lastUpdated = new Date(0);
  }

  GlympseUser.prototype.getName = function() {
    return this.name || this.identity || "Unknown";
  };

  GlympseUser.prototype.isActive = function() {
    var active = false;

    $.each(this.invites, function (invite, trackingLayer) {
      if (trackingLayer.active) {
        active = true;
      }
    });

    return active;
  };

  GlympseUser.prototype.hasLocation = function(latlng) {
    return ($.grep(this.locations, function (location) {
      return (latlng.on.getTime() === location.on.getTime() &&
        latlng.lat === location.lat &&
        latlng.lng === location.lng);
    }).length > 0);
  };

  GlympseUser.prototype.addLocation = function(latlng) {
    var self = this;

    this.locations.push(latlng);
    this.locations.sort(function (a, b) {
      if (a.on.getTime() < b.on.getTime()) {
        return -1;
      } else if (a.on.getTime() > b.on.getTime()) {
        return 1;
      } else {
        return 0;
      }
    });

    if (this.marker) {
      this.marker.setLatLng(this.locations[this.locations.length-1]);
    } else {
      this.marker = L.marker(this.locations[this.locations.length-1], this.options.marker);
      this.layer.addLayer(this.marker);

      this.marker.on("popupopen", function(event) {
        $(event.target._icon).toggleClass("selected", true);
      });

      this.marker.on("popupclose", function(event) {
        $(event.target._icon).toggleClass("selected", false);
      });

      this.layer.on("click", function() {
        self.marker.openPopup();
      });
    }

    if (this.path) {
      this.path.setLatLngs(this.locations);
    } else  if (this.locations.length > 1) {
      this.path = L.polyline(this.locations, this.options.path);
    }
  };

  GlympseUser.prototype.merge = function(other) {
    var self = this;

    console.info("Duplicate user, so merging together");
    this.invites = $.extend({}, this.invites, other.invites);
    $.each(other.locations, function(index, location) {
      if (!self.hasLocation(location)) {
        self.addLocation(location);
      }
    });
  };

  /**
   * Watch
   *
   * Set a timeout to update location
   * @return {void}
   */
  GlympseUser.prototype.watch = function(invite, options, container) {
    this.invites[invite] = {
      active: true,
      container: container,
      invite: invite,
      last: 0,
      next: 0,
      options: options,
      properties: {}
    };

    if (!window.plugin.glympse.containers[container.identity]) {
      window.plugin.glympse.containers[container.identity] = container;
    }

    this.setOptions(options, invite);

    if (!map.hasLayer(this.layer)) {
      container.addLayer(this.layer);
    }

    this.track();
  };

  /**
   * Unwatch
   *
   * Stop refreshing position.
   * @return {void}
   */
  GlympseUser.prototype.unwatch = function(search) {
    var self = this,
        invite;

    if (typeof search === "string") {
      invite = search;
    } else {
      $.each(this.invites, function(trackingLayer) {
        if (trackingLayer.container.identity === search.identity) {
          invite = trackingLayer.invite;
        }
      });
    }

    if (invite == null) {
      console.warn("Unable to find invite", search);
      return;
    }

    this.invites[invite].active = false;
    if (this.invites[invite].container.hasLayer(this.layer)) {
      console.log("User being removed from map");
      this.invites[invite].container.removeLayer(this.layer);
      $.each(this.invites, function(trackingLayer) {
        if (trackingLayer.active) {
          console.log("re-adding user to map");
          trackingLayer.container.addLayer(self.layer);
        }
      });
    }
  };

  /**
   * Track
   *
   * Trigger location update for the individual
   * @param  {Number} next Glympse next revision
   * @return {void}
   */
  GlympseUser.prototype.track = function() {
    var self = this;

    clearTimeout(this.timeout);
    this.timeout = false;

    $.each(this.invites, function(invite, trackingLayer) {
      if (!trackingLayer.active) {
        self.redraw.bind(self);
        return;
      }

      $.getJSON("//api.glympse.com/v2/invites/" + invite, {
        next: trackingLayer.next,
        oauth_token: window.plugin.glympse.token
      }).done(function(response) {
        if (response.result) {
          if (response.result === "ok") {
            self.handleResponse.call(self, response.response);
          } else if (response.result === "failure") {
            console.warn(invite + " is no longer sharing location.");
            self.redraw.call(self);
            self.unwatch.call(self, invite);
          }
        }
      });
    });
  };

  /**
   * Handle Response
   *
   * Callback to AJAX request for location, updates location and metadata.
   * @param  {Object} response [description]
   * @return {void}
   */
  GlympseUser.prototype.handleResponse = function(response) {
    var data = {},
        trackingLayer = this.invites[response.id],
        latlng,
        lastUpdated;

    // Update last known revision
    if (trackingLayer && response.next > trackingLayer.next) {
      lastRevision = trackingLayer.next;
      trackingLayer.next = response.next;
    } else if (!trackingLayer) {
      console.warn("Cannot find tracking layer for user", this.name, response.id);
      return;
    }

    if (this.marker) {
      $(this.marker._icon).toggleClass(this.status, false);
    }


    if (response.last > trackingLayer.last) {
      trackingLayer.last = response.last;
    }

    // Update metadata
    if (response.properties) {
      $.each(response.properties, function(index, datum) {
        data[datum.n] = datum.v;
      });

      trackingLayer.properties = $.extend({}, trackingLayer.properties, data);
      this.properties = $.extend({}, this.properties, trackingLayer.properties);

      if (!this.name && data.name) {
        this.name = data.name;
      }

      if (!this.identity && data.owner) {
        this.identity = data.owner;
      }
    }

    // New location, so update
    if (response.location) {
      lastUpdated = new Date(response.location[0][0]);
      console.info(this.name, trackingLayer.invite,
        "updated at", window.plugin.glympse.timeSince(lastUpdated.getTime()),
        "with location", response.location[0][1], ",", response.location[0][2]);

      latlng = {
        on: lastUpdated,
        lat: response.location[0][1]/1E6,
        lng: response.location[0][2]/1E6
      };

      if (!this.hasLocation(latlng)) {
        this.addLocation(latlng);
      }

      if (lastUpdated.getTime() > this.lastUpdated.getTime()) {
        this.lastUpdated = lastUpdated;
      }
    }

    this.invites[response.id] = trackingLayer;

    if (this.identity && !window.plugin.glympse.people[this.identity]) {
      window.plugin.glympse.people[this.identity] = this;
    } else if (!window.plugin.glympse.people[this.identity].invites[response.id]) {
      window.plugin.glympse.people[this.identity].merge(this);
      return;
    }

    if (!this.timeout) {
      this.timeout = setTimeout(this.track.bind(this), 1500);
    }

    if (trackingLayer.properties.completed || trackingLayer.properties.end_time < Date.now()) {
      this.unwatch(response.id);
    }

    this.redraw();
  };

  /**
   * Redraw
   *
   * Update marker and popup details.
   * @return {void}
   */
  GlympseUser.prototype.redraw = function() {
    var diff = (Date.now() - this.lastUpdated.getTime())/1000;

    if (this.marker) {
      $(this.marker._icon).toggleClass(this.status, false);
    }

    if (diff < 120) {
      this.status = "fresh";
    } else if (diff < 300) {
      this.status = "stale";
    } else if (diff) {
      this.status = "old";
    }

    if (!this.isActive()) {
      this.status = "expired";
    }

    this.popup = "<div class=\"glympse-popup " + this.options.popupClass + "\">";

    if (this.properties.avatar) {
      this.popup += "<img src=\"" + this.properties.avatar + "\" />";
    }

    if (this.properties.name) {
      this.popup += "<h2>" + this.properties.name + "</h2>";
    }

    if (this.properties.message) {
      this.popup += "<div class=\"glympse-message\">" + this.properties.message + "</div>";
    }

    if (this.lastUpdated) {
      this.popup += "<div><strong>Last Updated:</strong> <abbr title=\"" +
        this.lastUpdated.toString() + "\">" +
        ((this.status === "expired") ? "Expired" : window.plugin.glympse.timeSince(this.lastUpdated.getTime())) +
        "</abbr></div>";
    }

    $.each(this.invites, function(trackingLayer) {
      if (!trackingLayer.active) {
        return;
      }

      this.popup += "<div><strong>Expiration (" + trackingLayer.invite + "):</strong> ";
      this.popup += "<abbr title=\"" +
        new Date(trackingLayer.properties.end_time).toString() + "\">" +
        window.plugin.glympse.timeSince(trackingLayer.properties.end_time) +
        "</abbr>";
      this.popup += "</div>";
    });

    if (this.properties.destination) {
      this.popup += "<div><strong>Destination:</strong> <a href=\"#\" data-target=\"" +
        this.properties.destination.lat + ',' +  this.properties.destination.lng +
        "\">" + this.properties.destination.name + "</a></div>";
    }

    if (this.properties.eta) {
      this.popup += "<div><strong title=\"Estimated Time of Arrival\">ETA:</strong> " +
      window.plugin.glympse.timeSince(Date.now(), this.properties.eta.eta) + "</div>";
    }

    this.popup += "</div>";

    if (this.marker) {
      this.marker.bindPopup(this.popup);
      $(this.marker._icon).toggleClass(this.status, true).css("background-color", this.options.icon.color);

      if (this.options.showName && !$(this.marker._icon).text().length) {
        $(this.marker._icon).toggleClass("above", true).html("<div>" + this.properties.name + "</div>");
      }
    }

    if (this.path && this.options.showTrail && !this.layer.hasLayer(this.path)) {
      this.layer.addLayer(this.path);
    }
  };

  /**
   * Set Options
   *
   * Update the existing options
   * @param {Object} options Config options hash.
   */
  GlympseUser.prototype.setOptions = function(options, invite) {
    this.invites[invite].options = options;

    if (!this.options || options.override) {
      this.options = options;

      if (this.marker) {
        this.marker.setIcon(this.options.marker.icon);
      }
    }

    if (this.locations.length) {
      this.redraw();
    }
  };



  /**
   * Glympse Group class
   *
   * Manage tracking a group tag.
   * @param {String} tagName The group tag name
   * @param {Object} options Configuration options
   */
  function GlympseGroup(tagName, options) {
    /**
     * Glympse group tag
     * @type {String}
     */
    this.identity = tagName;

    /**
     * Name to use for layer chooser
     * @type {String}
     */
    this.name = tagName;

    this.options = options;

    /**
     * Is tracking active
     * @type {Boolean}
     */
    this.active = false;

    /**
     * Members
     * @type {Object}
     */
    this.members = {};

    /**
     * Glympse invite "next" revision identifier
     * @type {Number}
     */
    this.revision = 0;

    /**
     * Timeout to refresh position
     * @type {Number}
     */
    this.timeout = null;

    /**
     * MapLayer
     * @type {FeatureGroup}
     */
    this.layer = L.featureGroup([]);


    window.plugin.glympse.groups[this.identity] = this;
  }

  /**
   * Start
   *
   * Begin tracking.
   * @return {void}
   */
  GlympseGroup.prototype.start = function() {
    this.track();
    this.watch();
  };

  /**
   * Stop
   *
   * Cease tracking.
   * @return {void}
   */
  GlympseGroup.prototype.stop = function() {
    this.unwatch();
  };

  /**
   * Watch
   *
   * Set a timeout to update group membership
   * @return {void}
   */
  GlympseGroup.prototype.watch = function(automated) {
    if (automated == null) {
      automated = false;
    }

    if (!automated) {
      this.active = true;
    }

    if (this.active) {
      this.timeout = setTimeout(this.update.bind(this), 5000);
    }
  };

  /**
   * Unwatch
   *
   * Stop refreshing group membership.
   * @return {void}
   */
  GlympseGroup.prototype.unwatch = function(automated) {
    if (automated == null) {
      automated = false;
    }

    clearTimeout(this.timeout);

    if (!automated) {
      this.active = false;
    }
  };

  /**
   * Refresh
   *
   * Update the list of members associated with the group tag.
   * @return {void}
   */
  GlympseGroup.prototype.track = function() {
    var self = this;

    this.unwatch(true);

    $.getJSON("//api.glympse.com/v2/groups/" + this.identity, {
      branding: "true",
      oauth_token: window.plugin.glympse.token
    }).done(function(response) {
      if ((typeof(response.result) !== "undefined") && (response.result === "ok")) {
        self.handleGroup.call(self, response.response);
      } else {
        console.warn("Unexpected response from group", self.identity, response.response);
      }
    });
  };

  /**
   * Handle Group
   *
   * Callback for Init
   * @param  {Object} response AJAX response data
   * @return {void}
   */
  GlympseGroup.prototype.handleGroup = function(response) {
    var self = this,
        currentUsers = {};

    // Add new/existing users
    $.each(response.members, function (idx, member) {
      self.addUser.call(self, member.id, member.invite);
      currentUsers[member.id] = member.identity;
    });

    // Find stale users
    staleUsers = $.grep(Object.keys(this.members), function(member) {
      return (currentUsers[member]);
    }, false);

    // Remove stale users
    $.each(staleUsers, function (idx, staleMember) {
      self.removeUser.call(self, staleMember);
    });

    this.watch(true);
  };

  /**
   * Update
   *
   * Poll the group for events
   * @return {[type]} [description]
   */
  GlympseGroup.prototype.update = function() {
    var self = this;

    this.unwatch(true);

    $.getJSON("//api.glympse.com/v2/groups/" + this.identity + "/events", {
      next: this.revision,
      oauth_token: window.plugin.glympse.token
    }).done(function (response) {
      if (response.result && response.result === "ok") {
         self.handleEvent.call(self, response.response);
       } else if (response.result === "failure") {
         console.warn('Glympse: Group either invalid or all members have left');
         self.revision = 0;
       }
    });
  };

  /**
   * Handle Event
   *
   * Callback for Update
   * @param  {Object} response AJAX response data
   * @return {void}
   */
  GlympseGroup.prototype.handleEvent = function(response) {
    var self = this;

    this.watch(true);

    if (this.revision === response.events) {
      return;
    }

    if (response.events) {
      this.revision = response.events;
    }

    if (response.type === "events" && response.items.length > 0) {
      $.each(response.items, function (idx, event) {
        switch (event.type) {
          case "invite":
            self.addUser.call(self, event.member, event.invite);
            break;

          case "leave":
            self.removeUser.call(self, event.member);
            break;
        }
      });
    } else if (response.type === "group") {
      this.handleGroup(response);
    }
  };

  /**
   * Add User
   *
   * Add a User to the group
   * @param {String} id     Glympse user ID
   * @param {String} invite Glympse view invite associated with the user-group relationship
   */
  GlympseGroup.prototype.addUser = function (identity, invite) {
    if (window.plugin.glympse.people[identity]) {
      user = window.plugin.glympse.people[identity];
    } else {
      user = new GlympseUser();
      window.plugin.glympse.people[identity] = user;
    }

    user.watch(invite, this.options, this);
    this.members[identity] = invite;
  };

  /**
   * Refresh
   *
   * Refresh a group user
   * @param {String} id     Glympse user ID
   */
  GlympseGroup.prototype.refresh = function (id) {
    if (window.plugin.glympse.people[id]) {
      user = window.plugin.glympse.people[id];
    } else {
      user = new GlympseUser(this.members[id], this.options);
      window.plugin.glympse.people[id] = user;
    }

    user.addTo(this, this.members[id]);
    user.track.call(user);
  };

  /**
   * Set Options
   *
   * Update the existing options
   * @param {Object} options Config options hash.
   */
  GlympseGroup.prototype.setOptions = function(options) {
    this.options = $.extend({}, this.options, options);

    $.each(this.members, function(identity, invite) {
      if (window.plugin.glympse.people[identity]) {
        window.plugin.glympse.people[identity].setOptions(options, invite);
      } else {
        console.warn("Unable to update group member options");
      }
    });
  };

  /**
   * Remove
   *
   * Delete the group and remove all members from it.
   * @return {void}
   */
  GlympseGroup.prototype.remove = function() {
    var self = this;

    $.each(this.members, function(identity) {
      self.removeUser.call(self, identity);
    });
  };

  /**
   * Remove User
   *
   * Remove user from the group.
   * @param  {String} user Glympse user ID
   * @return {void}
   */
  GlympseGroup.prototype.removeUser = function(user) {
    if (this.members[user]) {
      if (window.plugin.glympse.people[user]) {
        window.plugin.glympse.people[user].unwatch(this.members[user]);
      }

      this.members[user] = false;
    }
  };

  /**
   * Has Layer
   *
   * Wrapper to underlying Leaflet function
   * @param  {ILayer}  layer Leaflet layer
   * @return {Boolean}       True if the layer exists within this container group.
   */
  GlympseGroup.prototype.hasLayer = function(layer) {
    return this.layer.hasLayer(layer);
  };

  /**
   * Add Layer
   *
   * Wrapper to underlying Leaflet function
   * @param  {ILayer}  layer Leaflet layer
   * @return {void}
   */
  GlympseGroup.prototype.addLayer = function(layer) {
    this.layer.addLayer(layer);
  };

  /**
   * Remove Layer
   *
   * Wrapper to underlying Leaflet function
   * @param  {ILayer}  layer Leaflet layer
   * @return {void}
   */
  GlympseGroup.prototype.removeLayer = function(layer) {
    this.layer.removeLayer(layer);
  };



  /**
   * Glympse Layer class
   * @param {String} tagName Glympse invite or group tag
   * @param {Object} options Display options
   */
  function GlympseLayer(tagName, options) {
    this.identity = tagName;
    this.isGroup = (tagName.substring(0, 1) === "!");
    this.options = window.plugin.glympse.parseOptions(options);
    this.layer = L.featureGroup([]);

    if (this.isGroup) {
      this.identity = tagName.substring(1, tagName.length);
      this.container = new GlympseGroup(this.identity, this.options);
      this.addLayer(this.container.layer);
      this.container.start();
    } else {
      this.container = new GlympseUser();
      this.container.watch(this.identity, this.options, this);
    }

    this.layerName = "Glympse: " + this.identity;

    window.addLayerGroup(this.layerName, this.layer, true);
    window.plugin.glympse.layers[this.identity] = this;

    setTimeout(this.update.bind(this), 3000);
  }

  /**
   * Update
   *
   * Update the name in the layer chooser.
   * @return {void}
   */
  GlympseLayer.prototype.update = function() {
    if (this.container.name !== this.layerName) {
      var enabled = window.isLayerGroupDisplayed(this.layerName);

      map.removeLayer(this.layer);
      layerChooser.removeLayer(this.layer);
      this.layerName = (this.isGroup) ? "Glympse Group" : "Glympse User";
      this.layerName += " " + this.container.name;
      window.addLayerGroup(this.layerName, this.layer, enabled);
    }
  };

  /**
   * Get Name
   *
   * Return the tag name for the layer
   * @return {String} Tag name
   */
  GlympseLayer.prototype.getName = function() {
    return (this.isGroup) ? "!" + this.identity : this.identity;
  };

  /**
   * Set Options
   *
   * Update the existing options
   * @param {Object} options Config options hash.
   */
  GlympseLayer.prototype.setOptions = function(options) {
    this.options = window.plugin.glympse.parseOptions(options);
    this.container.setOptions(this.options, this.identity);
  };

  /**
   * Remove
   *
   * Delete the layer and remove all of its children.
   * @return {void}
   */
  GlympseLayer.prototype.remove = function() {
    if (this.isGroup) {
      this.container.remove();
      this.removeLayer(this.container.layer);
      delete window.plugin.glympse.groups[this.container.identity];
    } else {
      this.container.removeFrom(this);
    }

    delete this.container;
  };

  /**
   * Has Layer
   *
   * Wrapper to underlying Leaflet function
   * @param  {ILayer}  layer Leaflet layer
   * @return {Boolean}       True if the layer exists within this container group.
   */
  GlympseLayer.prototype.hasLayer = function(layer) {
    return this.layer.hasLayer(layer);
  };

  /**
   * Add Layer
   *
   * Wrapper to underlying Leaflet function
   * @param  {ILayer}  layer Leaflet layer
   * @return {void}
   */
  GlympseLayer.prototype.addLayer = function(layer) {
    this.layer.addLayer(layer);
  };

  /**
   * Remove Layer
   *
   * Wrapper to underlying Leaflet function
   * @param  {ILayer}  layer Leaflet layer
   * @return {void}
   */
  GlympseLayer.prototype.removeLayer = function(layer) {
    this.layer.removeLayer(layer);
  };


  /**
   * Plugin Container
   * @type {Object}
   */
  window.plugin.glympse = {};

  /**
   * Icons
   *
   * Hash of icon data-uris.
   * @type {Object}
   */
  window.plugin.glympse.icons = {
    glympse: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAABigAAAYoBM5cwWAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAN7SURBVDiNrZVdaFtlGMd/z0maNEmXps5+KHNrzxpzMweTjHYUtbKxKtILHe1FS+tNRYcg6xhsKIIXflxsVkWEIU6ccxsWHCibUOfm1G5SFix+xFEtYVbrFPdRs3Vpm3Pex4s0p00rXogvvHB43+f8zvP5P6KqLF0inb6wXdZmIUlFkiKaBFCVlKApg6ZuZvJDqoPusneXAqN2T8KIHhShadmXFi1VRiyVR7OZQ2OLz60Fr0Qq4t39auloERYoD1FTb3NncwuNG5upuu12xLJAhI3tDzeppaMV8e5+EZEix198iDR27RCVAYCyYJD1mx8gsekeLJ+vxDM3n+f6lcvEauu4MHwmND11bSDS2AXwihdy1O5JqKWjQKii6ha2Pv4U4Wjlv0UMwOjQcdKfnwLIiZEN2cyhMb9Ipy9iBw4KhPyBIK09fYSjleSyfzGR/pbpqSly17NEYjFq7Tg19TY+fyGw+vV3F4GhQt47W/xhu6ytmLN1rVsoKy/ns3ff4rcfL6DGlHiU/uI07Tv2EAyFuTQ+xuTYD96dCE1hu6zNbyHJ4kmsto7TB95gQ6KOySUwgEAoxNnB97g6+Qv/1G4WkvQrkhSUQLCc5qqrHD31Ij9PXqH5oadLjCujYWpWrkB1ltjqahRQY/j10jXyTqEdFUn6i027IuTjyd7NGDWcOJkqgcUb6hg6sgfHdXCc0j03l6e1Yy+OaxDRpNeHM7N5HNfFdRxyM3MlwKrKCMa4GGMwxqBGvS1AJBz0bP2qkhLR9umbs+Rys5QHA2y97y727T/uGZ3/JkPHE69jr65GVfH5LHb2bQEUo0r2xkwhZJWUJagX35lzaRzHoeGOW1mXWOUBVZXR7y7ywYnzHPs4xe9/TGGJYIzh6+8vegUSNGWZRcDnX/twPjd53n75MeINtcsquXZNDfue7cQ1LsY17N3/iXdn0JRAhy9iB84We7F3Wwu7tj/oGX2VGufT4TSRcJDeRzYRiQRwXYPrurwzOMyB98/NR8HIdGauZdnoiQh9Xfeyved+UFAKyTdaKIhxDa5xOXxshDePfokxCotGz5Ovinh3f1EcANasWslLu7extr56HqgYY8hM/MlzAx+Rmbi8kGPRnTd+OrwgDlCQr3nFeQEIFY0tS4hFw4jAVDaH65ZMUE5Fn5keP/KqzoP+d4FdBix4+99/AX8DNhvIBWYvttEAAAAASUVORK5CYII=",
    glympseHover: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAABigAAAYoBM5cwWAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAN4SURBVDiNrZVtaJVlGMd/17Ozc3Ze9tJ0s2Ie3ZljGhoIJ6YYriBaEvYhmR82plQm2qcmkVEEEYUftJX4IQuKYmglJBUqWesFcprt4IocaBuHtWY6etHNHc+289z31YdzzrOdHehDdMMND899Pf/7+v+v/3U9oqosXCJbS0Kx0lYHiSsSF9E4gKokBE1YNHErmTmteswUfbsQsCLW2WRF3xehueimeUuV847K9slkz+X57525rEQijR1d6uhAHqyyzGV9dIIn4lfoXHuV1Uum8DmKAPseGm5WRwcijR1dIiJ5HF/+Ibyi/WlR6QaI+A3Ptoyws3mMUqeQwbTrkPw7yKraFG9+Xxf8baKsO7yiHeB1j3JFrLNJHR0AgtGqaU49NsAd5TP/xhiAl7+KcbAvCpAWK2snkz2XBdpKwjF/nwjNYb/h88cvcFdtims3A5y4tJixiTLGb/q5s3KGjcuvsy46QcBnAbg4HqHlrbinaSo5u8EXipW25jXruneU8oCh/cM19A5VY1QKMjrUt5RzT/VTFczwTbKa3qFq70yE5lCstNXnIHEAAVbVpNjccw8NK+/G/DJYRLEq6LLrk5X8+Hs5dsFlAA4S9ykSF5TyMsOl2r388PUj/HrlL9Y9/HxBcGVFiOpFFUyqUh9VFFBrGbt6nYybtaMicV/etKXBSnZu34RVy8kvEwVgjfW3c/roc7jGxXUL9+xshvva9uMai4jGPR9Oz2RwjcG4Lunp2QLA2yrDWGuw1mKtRa16W4BwKODF+lQlIaKbU7dmSKdnKAv4ebBlDQcOn/CC+n9K0rbrELFoDapKSYnDnh0PAIpVZXJqOldpSTiCevy+PTuI67rUL13M6qY6D1BVGfh5hI9P9nP8VIJr4zdwRLDWcuHiCPn2FTTh2HmArxz8NKdNhndfe5LG+iVFlWxYVsuBF7dirMEay/7DX3hnFk0UGBtg25YNPLN7kxd0LjFM75lBwqEA2x5dTzjsxxiLMYb3jp3hnY/O5lhkjV3UeiLCjvaN7O68HxSUrPhWswWxxmKs4cjx87z9wXdYqzC/9fL8I40dXfnhALCsbhH79m6hYXlNDlCx1pIc/YOXuj8jOfrnnMaie6aGjswNB8iOr9zEeRUI5oMdR6iqCCECNybTGGPnS5pW0RdSw0ff0BzQ/z5giwCz2f73X8A/p73Upg67GLcAAAAASUVORK5CYII=",
    glympseRetina: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADEwAAAxMBPWaDxwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAelSURBVFiFrZl9bFPXGYefc+3EhnzZISQkJKWMhM/QwiBUlP1RiWorpWwltLQjpBRVFDGtUG0CsYpWaNoEa0VhQpRBtbElkG4sbUoHRd2GurUIiW/UwWCQECAlwZCQxHY+7XvO/rDj2Pdex0ngF72K7zn3nPfx+T6vhVKK4chV9PKEIEk/EqhZKPJA5YHIC+WqRhCNCBoV4qxN8Vl7bWXtcPyIoQCmT1qWpaT2BopSoHiIvv4L1Cgpd/qvV3keKqAoWDoizZn8plJsBNKHCGaUD/iNv6f3fdVwsOuBAVMKly/QYC+Q/4BgRn0r4fWO2v1HB3pJGygzvbB8vQaHeYhwpdPusqT4LkC+BofTC8vXD/S+ZQuKomcdaSrzQwXl8Qo67ZKpOX6mZncwLaeDqdl+pmR34LBLPD4HHn8yHn8ylzwpfHopm/rWEWhCcWLNaW60juDlj6b3+4NKn7i/Sl37vGdQgOmFyyviwQlg2Ywm3plfT1ZK70BfPkbnbqdz5d5Ils24Q1AKpr7/JC2dSTGQ3tr9ryQETC8sX69Q71o5mZ3vZesz15iZ5xs0WDxt+LyI358ZGwuD2OCtrXwvLmB4QhzGMDY1odi28Crl321CPDBaSKcaMliwb6YxWUp4LnriREDE+JVODfYY4QDenl/PKw8RDqCkoJ1HXN3GZE2DvaJg6Yi+BHvfhzStd51CFBhLLJ3uYe2Ttyyd6ErQ5HVws81Jo9eBEJA1spfxmV3kZ/RgE/GXMAEsKfaw/fg4Y1Z+qiP5Z8CvIdzF6VNWjFIBvQ7IiH5zZp6PI6+ex2GXkbTOgI2aS9lUnsvlfGMaQWndrk67ZEmxh1VzbjN9jN/ynSv3Upi3u8Qqy6ekLPJfr/LYAVRA/sQIpwnFvhcvReC+uZNKxdk8qi9m4+uxm6s0qDuoceBCLqNTA5aAN9ucfFnnjlc8TWjaG8CmkCelSo0DbO4j7RRkdNMVsPHWF4VUnMtNCGXy4gjy07kNEeDjN1wcq83kWF0mdS0jExVfDGwSGZPKx+tBed2Y+96zV/neo22sOFjM1eZQZclJdnoDwUEDLpzczNxH2jhWl8mJmy56ggNuXCZpiCJ7UNefF4bmswnFzDwfiysfp0u4KSudzaY3FzMm24178quDdnDkShZHrmQNCSpauuCHdoGYbcyYkt3Bxi+fYNeOtcwrmYwmBJqmceFS/bCdDUcCNcseOmzG6j4FnDr4K4SmoaREaRpSSir++lXCSke503hx0RzG549GlxKlVMikRCoFSiHDz7pU/P2ri5y7eAtdl+bKFHn28Ek4Jn3qxLFIpdCkQmkgpUQIwdcnLw8I9/yCEnZsXo6UMsqU4Tlkevj/c08/htfXxTPl2zEfC1Se1n9M71dKihMlFUrFOmm+H38PFkLw9rrFodaJGFGfY40oSx2ZzOQJY6xqzbOcVlKXJjgpJc7k+OtfcpKNTNdIlFQGyLAl+JsxzbSJAaCFLjix8txrD4MppJIhk5JJE0yNHVFPb5DqI6dQSkZByjgtiMnuNlvtNqpRA2ECbGn1xYD12ROzCuMCAmze9jEV1ccNkH2TZOCuth4+otGOoBHD4Gz0tCGlRBMCJQRChFbKhfNn8O6uv8UF7Ojs4Zfba9iy8zOcziRTvhCCih2vkZvtisD1gTY03bfgo1FTqDPG9K7uXv5z+ZZp5o3NcfP9px4bsBUBAkEdn7/bZNmj0sjLcZlasdXbyf22TlM9CnFWSwrKGisnHx74l+XysGNzGakpzoSQRtlsGls3vhBZHZTqHwJVNScty9gJHNJa6z+6AeK8MfOfX1+ksysQAtP7ATUh2LN1JU6HuQvjyW7T2Lt1BQVj3VHjsR/y0D++sSp2se3an+s0ACGoNuYGdckvtvzFtLBKKZlZPI4Th96h5PHvJITLzXFR+dtVFE8eG544MryThFpvd+W/6e4JmAsKPoHwgdU1rsytJ4k6IOaAJgRU7VpD8cT8yGYTmi4qNK8UHD99jZqjZ7hc18Tde+3ouiI1xUFudgary55iXkkhMnpWh7c8KSVNd9tYvvYPBM3bnFfY5ATv/6qaI5emeLc5p8POFwfW48qwOL8pwqDh5da0e8hIl0bDKSnp7g2yZPXv8PlN9xKE4C3vtf1bIOqC5NPtO4EG48vdPUFKV+2k3dtpOWmk1GP2XSVlxKIX+2i43qDOyp//0RIO+NbX3buj7yECqOr3dUtYDZjau6XVzw/KtnH1+p04kNamog8MYcg7zV5KV+3m9p02Kzgp4fXooNKQLu42m8ZLi+aw7rWn0URoUKpwP4e6mJjFt8/6lpOK6hNUfHySQFC3qj7xxb0fMn7oA8DpSOKlRXMoXzKX1JRka8DwmOzqDlB95AxVh07j7zCFXqLgBhn6gMEFj/qU5U6laEIOuaMzKMh1Y7dr3LrdQqOnnbqb9/A0+0gU4hty8KhP4e7eSoIw3QNICsRGY7dGa7ABzD2A9YFt+GqQsPqBApgAHbX7j/r1pIkCsQFofQhg7UKpjX49aWIiOBhiEN01rswtk7U1SvECKFNoakApLiDEJyJJ+8B7+U8tgy02JMBoucf/+NGAXVssELMH/hlCnbHbbJ+2XakY1p31/3HKL9rbmHsZAAAAAElFTkSuQmCC",
    glympseRetinaHover: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADEwAAAxMBPWaDxwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAnKSURBVFiFrZlpkBXVFcd/575+jzcM46AwrCqMsigiuITBiUmJRsVtLNxSWhUSNCpaMRqX5ItVLtEPMS6lQulgZbGCBkqLGMUYYzSTuDCCQYVYxAVEkGVYBQZGmNf3nHzo5fWbN6BS6Vf3dd+eft2/+z/LPbdHzIyD2U4//e5gXZ/6CQ6a1XQU5sepMdFMQXWZGSsMvxLv2xtzLG9ruzM8mOfINwEUQY45/7FmTG8w7AozAzPMDDMF0+hYFbO4lY/nee9nb2i/t92Mr/3Qrw04vqW1yYvNF6PRKINBAmS9QZWP4716Xe2kdPnni+5b8n8BHH3erIZCvs9CYHJ0JgOWKrcf1ar2PttfrN1dLRvffXTLQQOOv6i1CXVvIRIgQhmQsjm/QrFo79N+w4A6fFhiy9admGloGp668Z2H9qvmfgGPm/bETJBWEUFEAAGByLoGpuQLjtqagMED+9F41BCOP/5oisUC69dtpqNjK1u27OTztR2s+WwT3WGIqXLbLZfxyYcreW7hYtR7TD2idt2GpQ/O+dqAx13YOhPnWkUcURPKCsKA/kVmTJ/CoCEDUIsUtUjc8o3j8YiAE2HRW++y6pP1XH3NhXgfMmP6HahJWWHT6zYtfbgKsgpwbEtrU+BksYhDXAzoHIijkHe0TB3PpMnj8GqoUQa0Cr4U0MWAzkFOhJyDnBNuu/nXdGzaWeGfTnXyxvcfXbJfwNHnzWoouGADORdEcLkU8uQTR3DpRc2YgFcqADUGrLhxql41YOCE1155k6eeehnV2E/VY2iYg2HZwHHZmwYiC00Ieso8dlQDl13ybRQoeatuYbTvDpXuUKPjHteE3gjVCBVKakw581REeozKCEKvCyuYUohzZjVJTiYnhhKLvg6pC7hqxpnRzeOHJAruC0PWrlrLmk/XsnnjZsyM/gMOZeSoRkaMHkkhCGIFidUmdYcgJxx55CA+W725Z9aePHD89U1bP3h8SQoogow5R+dDLh6JYRiFguPWGy+MRx61L77o5Ll5z/PB8o8wV0OhWEsuyEOarDex6PXllEp76VfMcda0szjplJMjuIwDiMC550/hsUfmxWqUZTSR+SJytJlZAHDM2Q81K7nGdHgYJsa1M86gUFMk9MaiN5bytxdfZ093jkKxjrpBY+JcWJ37xAXk8kVC8yxd/CETJp2EuThHxdtH//2Etr+/iVqIpMIkV1jjYeOvaQYWBQAl4QZnFo3EDFCKgWNk4zC279jNPbc/QLfVUlM3kGIgRFNdkpyt3Oi5h3MumUrJG127u2j/x79YvvR9tmzYissXcLkCLpePBhZ/kjGY5wZgkUyZclewLl9bEnGQpBVxHD9uMBMnHsWcx56ldsBwcq5APnCUQiUtEuJ5mP3Mw31rCvTtJ6z/9DO6SxAEBVwumpS873X6q+h/MWh4Pljn+k6wWD2JzYsoY0cP4/FZzzBsRCNnTBrJTdObGDTwEMacfT99aut7VDFWraYZnZ172LXTExQPwxXKU56axvZM5LIe/eirftOaCYGXUrNYEIWYUwDyuYDnF7zC3IdnMHniiDiPOZatWENSvaRFQwWcAlHZhVk8A7l0EBjlMO4FqtJFwJDmQExGgUbQ6sAp9UXhpbm3IM5F0juHqvL0c28T5GsyqiVVTWJ2pb42T8tpozhi6CF4VVSjv6t6VKNrVKO+D43XFn3Eeys2RARJDCXwyqhAvR8nLoekkMKYkYeiZjg1zIGqIiIs+c96RAqY+vg+lQXr1FNHcs+N34sBkmY9+lHz8f6c08awq/NLps38bWzZstkFHecgKtOzBWexmMfSkZcfsmNPCU1KJ/UVAeEwfvbD5nhuThpVfpm0rKn79S1w1PBDIInlOAOoMtGl0ZeU7KqoD6vgVJVC3lXWfpnoy+eFw+prMLUekBm/OsDnhGOHRUHawzedeV1mGlfE8cO2bNsdgxlqUdSpKqOOHJhCqVamhL37Svzpr/+O+wlkdWSXla1sm7btLqtXdptlztAVFYqYsn1HVwVY0k48dgioJ1shJ5DqPffN+SdzFyzqARk3PbCpt23vpBzOsYrYigCxlZhiCFhUmHZs242q4kSwuKIWYOp3x9I6fymSCyqiLYnmkvThwd+/xQNPvEqxT/ma8iLO+N390xk6qL5HgBmfd+yIk3cGHlY6H2p71rxmypf7PB98uK4q8oYP7s93Thy2nxkgakGhSNC3PyVXS0lq6JYauinSbX0YPnwIwwbXV6n4xa4utu/aW5UfFWt3G4qF5T2dXtXz5IIlvaaHX/3iAmqKUg1nlaBk7okZTuCun56ZZgezsgv88bklOJfLuFl0vuvwdcudtd0Zmtm8ChVVeWPp53y5txSB+TKgE7j/1rMpBIKZj4KlJ2QKGgVfTuCR21s4fGh9xh/LkC+8+kEa6cQJH5hnbW1hVFGrza5MGx41xx0PvVSVWFWVE44dzsu/+RETxjb0auYs6JCGWubcezHjxgyOA0crgufxp16n5MvTY3JeYDbEaxIRZMikn68SpDFZvUXLTWXOL6dx3Ojh6aozWXsmq7j291bzYtuHfLxmG5u3dqJe6VuTZ/CAWq685Fs0nzgCzUa1RoCqSsfmHUy/6UlUXKUlhNV7Vj59tJlZumgaOumWJnCLYzogitxAPH+ecxX96/tStaXLzcwMUDF7aGrSLJypsrc75NKZc9i9N6yqjJza5J2fPr0kVTDZBp9889siMjnVKlazXw08M+tK6voVewG0TCVChZk0C5qB7A49M255kg1bdlfBicniXavmnpI8omJV58y3RK8jIrk1nm87uzwX/PgxPv60o9fI3l+z9NhiMysdW3dxybWtbNxchkv8D7HQ3L6WLFPVwn3oCTc2eWFx4nRC+Y2CqGfa1GO5ccYZuFjdAynYU8k/LGhn7oIl+NQVytU5UGHa/QICNEz4yUyca624MAY1M5yU+P65J/CDi0+hX22hd8DYJ/fuLfHsX5Yy74V32NMVlt/vUAkHcl3nyrlf/eojC2nQmhGwAjcqtzyH9ssz+qhBDGmo44ihh5HLCWvXb2fjlp2sWrOVTdt2A5KCZKvwzP16hTsgIMDA8dc3meMtrPptQ7RZpKwY6jVd0yhgajgncfpIFlnZShwQC53n1J5m/dqAAHWjr2rIF/ILkfgFpqVfKaQl59KotorzvakmJovN7Wvp/OSZg3+Bmd3qx1/d5DzzERozbJSTtpH2UsWoWLsAmLA65+3yA6l2UIAAIiJ1x1zZ7OAGjCuyM0pSgZRhM6aMfj1PYHbnqqfa7Rs89BsBVvzw9LuD+nVrJngXNmOMwmwcxkQzEGfLUFbgWKlIe9fh65ZbW9tB/Rvif7u/zea0zCraAAAAAElFTkSuQmCC",
    male: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAolBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgESU6AAAANXRSTlMAAQIDBAUGCAkKCwwOFBUWGB4gIyYuLzE5PD9CRUxQUVdtb3N4iJGjtLzM197i5Ojp7/H1+56J1akAAACiSURBVDhP5dJHFoJQEAXRBiPmAGbMOQCitf+tOdCj8qEX4LGG/e6wRb6bxwQD0VsAMFL3EgBcLQ24T0BNA50XcDRgxwActV2kB3Br6kBam/26qmyuP3s37efNOXcgUdQwwAqjkwHOJqCSBKmdegp4b7Ur/CgIofsBNqmn8IKt9QHih0vJCEAusMsa/whEEMoRthqYwFiG3NsakHJZRJxi4vYAU0lCRr+D0b8AAAAASUVORK5CYII=",
    female: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAA/FBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADzfFO9AAAAU3RSTlMAAQIEBQYHCAkLDBARExQVFhcYGxweHyAiIyUpKjAxODw+P0JFRklMT1BRVldbZGdzdXh7f4uPlJWXm6Oor7CytLy+wc/X2d7o6evt8fP19/n7/dzWLVAAAAD9SURBVBgZdcGJIgJRAIbRfyqUhGyhjF12yZ59IpQlfe//LuqOmpnqnqMw5/iLelF2N3ScyGYWX1IWRXx5WWziW5RFCuMzJpsiba2C7BYuns4zshlb3zf2VuMaptCkqz6lQekmgfeEBlQJu1K/A6JWFJVuwb0wvNgr1BOKqEIzJQxPc8C1wg6BbcUx3hyVAFeBiRY8SGV8RcVq0Eio5xGaKU3y73tU88CZemqwIz3TdSmdQkU9M7dH0hqBnJxSZVwR8QaBF0cDyoTtql+GiJ8R9dkgKqd+edd1MWqu6y5pKAxPVhierDA8WWF4svqg405WW7T9Lssumc1OJxTxB3kHaL+trXjUAAAAAElFTkSuQmCC",
    car: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAxlBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADa+ousAAAAQXRSTlMAAQIDBAYICRASExYYGhwlKSswMThARkxOT1JWV1heYWJmaGl4eX5/iIuiqKqwsrq+w8rO19nc3uDm6fH19/n7/aKkBqkAAADnSURBVDjLzZPbTsJAFEU3VcEbCiIqiICAilbxUrEVELv+/6d8wKRzqQ1PxPV2stec2Zm00sbYvU8weLvetvM6LknFzHe+PYFnU2iRw74hPEFcMuZSDJxnc5DCyOp0Azxk4xFwYgkNYJntvASs0qoAHJgVPpx3sUr4FawSxy8pf/AVHkpVikirCiFq3+Vkj63hHCZaMA2kMy/vSSrPWAquJAWesCVJXVhHKLwCAVH7NqdkeDGYsxIKUVycx9JqR9N56ubvea0jvAPUHaEGQCRJ6gAz5ytXMDX29pPJnvejlMefr6f6H/wAc0JvqKo5trkAAAAASUVORK5CYII=",
    bicycle: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAABVlBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADLqr8DAAAAcXRSTlMAAQIDBAUGBwgJCgsMDQ4PEBESFBYXGBkcHh8gIiMkJicqLS8wMzY4OT0+QUNERUpMT1BRVFlbX2Jma29xdHV4eX5/iY6RkpSVnqKjpaiqrbC0t7y+xcfKzM7P0dPV19na3N7k5ujp6+3v8fP19/n7/Vj32cEAAAFxSURBVBgZvcFnV9NgAIbhO20io3EAIuKIVsWCiiKOShEVGSqOsmKlZahgqxbaPP//i28Gx3Nq4ze9Lv6J0Ur1In9hN6VmlnQjMoZJ5xxJLYfu3HL9MZe3tq/Q3WBd0gSpRn7K+G6T4lJLkXm6uxbIeDUjnaObvELLsKHQEp22ZSwD7pGMJTptypjCsE8YNp36KzKKpLMetiTN8qdc6VOzNjcA/W8kH8OaeF8/WLlJbDKQ2pKKwLR8wK1KgaTNHoy7ahSyZK5/VQk8+dD3TS9dGHitWhZyQSNHyNnRKJ58KOs+kTnNQ0kFYue1giefIa0Ss/bksKUsif3A8uRzTwUSRV3lsM2xNdmefJ7oAolbGqemDIkvsjz5TGmMxCPleaobxM7qA6FhvSVRUw+ntN9LyK7II7KuO0RmtAA80MFYhkx+V8+JuT/0LAenF/XZwZiW1JZU4tiZPSkIpI99RE7OVg93Xgzxm3W73Ki/G7f4L34BTQ5i6hB5zH0AAAAASUVORK5CYII=",
    truck: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAulBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACrJGrzAAAAPXRSTlMAAQIDBQYHCAsPEBESFBocHSwtLjE1ODtCS1ZeY2lrcHN3hYaJkZKUl6Cvt7m8wcPH1eLk5ujv8fX3+fv9nj6/RgAAAMxJREFUOMvVk9cSgjAQRVkRwYq9V1SwK/Z2//+3xAUVW8YnHe9LcpMzm91NIknf1A7PSvuBF/vIfg4oiiICihu8lgdUASFQwyEjyqGBjS5KsoRlVFiFjbi4TGAyZL0D9hDoX4AyT/KkjX3rC50iMw8YnMeWEyny8JBi54lzR8m2sULB7aRlsADNsQT06jL32OQIYRzJ7bmNFEe4eCkH5EgbwfJ8BfMEhacwr++tzwcvVc8GOD/M1StAlTWO3ZuXm1scOqG7TxEkSeh/qhP2sX2KqO7PIQAAAABJRU5ErkJggg==",
    thumb_tack: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAq1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0NbREAAAAOHRSTlMAAQIDBAUICQoLEBITFBcdICEiJCYoMT9ERkdbYWh4hZKUmpuepq2vsrS3vszP1dze4uTr7/H3/b3FXRAAAACwSURBVDjL7dFXDsIwEEXRARICCb0Tem8BQkJ5+18ZH1hiPLHMBrhfludIljxEqtb6xFpUSBRA7+EKEAqAjgB1MX+V5BuDK5+fu5TJBZJCogCREUQU/UEZSP1UgVxmHBz0XSyL+rz2lNu8eXyej5HpyMEQhtoM7E1gysDdBLYMrExgov3SJw9AqM6O6bPoAjTI1hzwrWAEuFbQM+/pWxWxHTjY2QGlsx9gM/4B+k1x8QbBM0skgoFQzQAAAABJRU5ErkJggg==",
    map_pin: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAz1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACZY6YEAAAARHRSTlMAAQIDBAUGBwgJCwwOERIVLzI0OD0/QEdMUVRbX2NmbG15e3+AgomMlJegoqOltLW3ubrAyMzP1dna5ujp6+3z9fn7/Zb54XIAAADJSURBVDjL7ZHHEoJAEAVnERTFnHPOOecEvP//Jg9aug7o0ZN97a7dqRkiCV+yOerXYiq5o1RtAAAuOVevL/FkpDi9Zw0cM34q3Atn0AZmmlGZmvc38tyHga0atZ6fXPmkRSAasl9TIM6COSzRljzqLDhiRXs5GLDghFNa9hiyYAFGgwVlHiRYEGHe1Pgieu9B0bFJbSf7sXDu2ti8/MT14mrroa2SIHeMbHc+6aS89BGhEmnis6cgAAT+wQ8Czxk4KF8CUnSd3eoGVP9KBFhozloAAAAASUVORK5CYII=",
    bolt: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAA4VBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzthe/AAAASnRSTlMAAQIDBAUGBwgKCwwOERITFRgdHyImKiwtNTY4Oz0/QURJTU9SVVZXXl9jZ2xvc3h8g4aRlZqdqq+6vsHFz9XX2dzm6O/z9fn7/ZfuWNoAAADPSURBVBgZfcF5OwJRGIfhX0V2ZUmWyE52shRKpMHz/T+QM2f+mevM9Z77Vs7eaeqkJkOdzJUMB2RWZHjC+5AlwTuWYZnMnAxtvJ4sXbxtWb5J/ZTlzGw0FFrEu9fs5s0QBgq18F5HeLsKPZA3Lik0Ju9IoXnykopCW+RdqOAOL+nj/FVV8AmTx/2amji3KtppLyl1jbMg2wDoyjaNU5etAbwo4hJYU8Q7vCliCmgqYh2GijmHlmL6fJUUUYZDxawyqSjmjI6ier9VRT13VPAPggM+7rK7CskAAAAASUVORK5CYII=",
    star: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAA9lBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABJZEqTAAAAUXRSTlMAAQIDBAUGBwgJDA0ODxAREhcZGhscICMlKCouNzw+QUJDRklQV1tdXl9vdHx/hoiMkZKUl5iio6aoqqutr7K0tbq8vsXHyszR1eDk6/H19/2wUcDwAAAA50lEQVQYGY3BC1cBQQAF4GsXqTaiqIhKpIT00kN6P1TE/f9/pl27e5oZO3P6Pig2kjDKcACjczIPgwzJOxic0rUOrRV6bqHV5UwOGg5919A4YSCLSKsM9WKQWM5m9bg/omB4drS9loJn5+qbOtO3zjKeaFTCwpAGDQCJd2rV4Ym/UqMGX/yFkfYRsh8ZYQ9/rAHnlCGKPVBRgaxLRQ6yTyrKkFhUNSFJU3UPSYmqH0gOGBgzlILogjPjmpW+pK8A0Rddk0MbrmyfnioENslpM4FA4ZlkCwKHbC9CUPzgDUS7S1Bs5fE/v381jRxdlWRsAAAAAElFTkSuQmCC",
    anchor: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAA8FBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABmRfStAAAAT3RSTlMAAQIDBAUGCAkNDg8TFBUXGBweICMkJikuLzQ2Nzg6QUdNT1BVVl1eZ2htb3FzdHd5g4WIjJKXmKCiqqutr7m+wMHFzM/R2uDr7e/3+fv9z3ramQAAAQJJREFUOE+l08lSwkAUheGTkCiKAziC8xQUnGeNIk4gmsT//d/GRSxtSLqskrPpxfmqb99FS2bcCyDelDUhAGzZ+sW0J/YsoAkdrwzMWcAhtOQBCxawBrQ6QNEC3G76hnNLL5W6ADeuFagG4Nt7VYcCJ/wmquYAzJz+Bc5ywLsJjnPA9E4QXAI0gmB7JAdIw675X+D2AEiKg2AlXUd+etYzNzihCfZzRjhPBnj1s2AmMUe8TAyCpehnxAMAe6MmmLwFiB+RT12N9Jbr5dIqwHh54xmAZN4JpYqk9U9y8jYlOd+vH7vP9gcFmalc9bUfzezf8GpH7Qigd7c768gS1yv0d18lC2jpKQbt1QAAAABJRU5ErkJggg==",
    bomb: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAABDlBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABxUYW9AAAAWXRSTlMAAQIDBAUGBwgJCwwODxEUFRYXGBkaGxwdIiUmMDEzNjs+P0FCQ0RFRkdKTFBRVFdZaHN3eICFiJGVnaOoq7K0ubrAwcPHyMrO19na3uDk6Onr7/Hz9fn7/Ua5IDcAAAELSURBVBgZlcEHUsJAAAXQvytoCGKhCIoNKxZU7KgUeyM0o/jvfxHHMRtTNs74HqJUs5IoVhAp0ysO5zopRJsqJw/yEn8wkOXQRKRFSx7nEG3BMpHuZaGIQrX+/P5SKxtAqT6GecsEMF3CD1HqUDnZIB9W2iY8zEcGpeExM6C9J3P0quGXOeB9AjDoY0IRT7yVwFKbXldwrbNnQF7QbwKK6HMVuGTADpRZdgU2GdSCcsiuHO0zRMLRIO0PhqXgsKiXg2NAvTwcd9Qbh+OcejE4lql1DcWg1jZcZ9SwY3AlPhlWgccaQ5oCXkcMeI3Db58+jTiCCjd0ve0KaBRPbX5rbsUQQRiTmeQI/u0Lv3ejBaqcOy0AAAAASUVORK5CYII=",
    building: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAilBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATAggvAAAALXRSTlMAAgQFBgcIDA0REhMUICUmJzk/QFFSVmhtcXN4goWJi46Pl5udqKqrubrKzu1TyU53AAAApUlEQVQ4jd2QuRKCQBQEB2FRFMUD1xMV76v///dM1KKoJaDUxMleTQfzWpLuuHPXMxU9fBdYKIJICw5qQF/zMmDlgy9LJkGstAzkJoHE5FzCDgyD1fdHTtWCpqbs5UFXk9rA5xty0y+8OQrWtUVtTAI9s+Ectv9Y1C4eFJpxvHWKesch6pYuIbPWWjuDbXp1bvBL9w+AliTJq+/hVNEfX4ACdyRJD3OIcmpNAEpPAAAAAElFTkSuQmCC",
    eye: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAABMlBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC88vZ+AAAAZXRSTlMAAQIDBAcICQsMDg8QEhMVFhcYGRweHyAjJCUmKCk1Nzg5Ojs8PUFGSUpMTVBRV1hZXmFiZmxvcXR1d3h5fn+AhYaJi4yOmJqbnaOlpqitvL7Iys7P0dfZ2tze4OTm6fHz9ff7/S2JgwQAAAFESURBVBgZ3cGJWhJhAIbRbyiYygGUFjY3LFuUMtTIfdcWl6g0DQRk/N/7vwX/wXEeKG9Az9Gdk8iXpqZK+YRulZpvEmrOp/SvzCF9DjPq9WCNrk9PnOeEVmOKpOpQN1zmJD0jYOpQTypU9DEzI/BaDxeaXBv+aPAL6soazjNaoaHYMTeW9bSJeSHL6/AnLh2zordEfkuJUy48yflFw5XkM6tNIh1JboOfjuYwg7L+0lr0iZzJGjLMqkVZgW/0+aLAe1o6YluBl/SZUGCHIyUNr2Q55/RoOLImMUmpjMnJytEjKysPZVkb+AVZ44aQGZVV9NlQ1x68kTVUo+tHWtY7+KrQAnx3ZaWnq9XptCx3H5YUGWtjqo8UefzZcFFSj/gWUPtQ8FzXK8zUgN24+g2sdoj4657+5xQrByft9slBpRjTPXMFyfd8QTOpicwAAAAASUVORK5CYII=",
    shield: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAilBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATAggvAAAALXRSTlMAAQIEBQYMGxwfIyczNDtVVl1fcXN0gqa3vMHDytHT1dfa3OTm6e/x8/X3+/0lK9V4AAAArklEQVQ4y+3OxxKCQBRE0UYQFQPmnAMGvP//ey4YRGRm78K7ejV9qkCShinW0oGyHDukBuDMBm5XkwuE5lGXP/hFkFjBHRJzrmzAB5bmnthABxibO7KBNRDln9tVQR/Y57s6FdAG6L6BZl8gSoFZsSs4lEDvCRz8D6DwXIDaFOAUqlR4zEFrA3D82qX6IgNzABZ1VfLGxZ+OPNlqbrN525AjL37AI/bkLojjoPzyAqJVWn8o79iJAAAAAElFTkSuQmCC"
  };

  /**
   * Default Options
   * @type {Object}
   */
  window.plugin.glympse.defaultOptions = {
    marker: {
      icon: L.divIcon({
        className: "glympse-member default male",
        iconSize: [16, 24],
        iconAnchor: [16, 32],
        popupAnchor: [0, -31]
      })
    },
    icon: {
      shape: "plain",
      image: "male",
      color: "#ffffff",
    },
    path: {
      color: "#00ff00",
    },
    popupClass: "",
    showTrail: true,
    showName: false,
    override: false
  };

  /**
   * People
   *
   * Hash for GlympseUser.
   *
   * Duck type: Glympse User ID => GlympseUser
   * @type {Object}
   */
  window.plugin.glympse.people = {};

  /**
   * Groups
   *
   * Hash for GlympseGroup.
   *
   * Duck type: Glympse Group Tag => GlympseGroup
   * @type {Object}
   */
  window.plugin.glympse.groups = {};

  /**
   * Layers
   *
   * Hash for GlympseLayer.
   *
   * Duck type: Glympse Layer Tag => GlympseLayer
   * @type {Object}
   */
  window.plugin.glympse.layers = {};

  /**
   * Containers
   *
   * Hash of elements that contain GlympseUser.
   *
   * Duck type: Container ID => GlympseLayer|GlympseGroup
   * @type {Object}
   */
  window.plugin.glympse.containers = {};

  /**
   * Control Button
   *
   * @param  {Object} Configuration options
   * @return {void}
   */
  window.plugin.glympse.controlBtn = L.Control.extend({
    options: {
      position: "topleft"
    },

    onAdd: function (map) {
      var container = L.DomUtil.create("div", "leaflet-glympse");

      $(container).append("<div class=\"leaflet-bar glympse\"><a href=\"javascript: void(0);\"></a></div>");

      $(container).on("click", function() {
        window.plugin.glympse.dialog();
      });

      return container;
    }
  });

  /**
   * Init
   *
   * Bootstrap the plugin
   * @return {void}
   */
  window.plugin.glympse.init = function() {
    if (localStorage) {
      var storedSettings = localStorage.getItem("iitcGlympse");
      if (storedSettings && storedSettings.length) {
        var settings = JSON.parse(storedSettings);

        if (Date.now() < settings.expiry) {
          window.plugin.glympse.token = settings.token;
          window.plugin.glympse.config = settings.config;
        }
      }
    }

    if (window.plugin.glympse.token) {
      window.plugin.glympse.showButton();
    } else {
      window.plugin.glympse.authenticate();
    }
  };

  /**
   * Authenticate
   *
   * Log into Glympse
   * @return {void}
   */
  window.plugin.glympse.authenticate = function() {
    $.getJSON("//api.glympse.com/v2/account/login", {
      username: "viewer",
      password: "password",
      api_key: "nXQ44D38OdVzEC34"
    }).then(window.plugin.glympse.authCallback);
  };

  /**
   * Auth Callback
   *
   * Callback for response to authentication request.
   * @param  {Object} response XHR Response data
   * @return {void}
   */
  window.plugin.glympse.authCallback = function(response) {
    if (!(response.response && response.response.access_token)) {
      console.warn("Glympse: Login Failed!");
      return;
    }

    console.info("Glympse: Login Successful!");
    window.plugin.glympse.token = response.response.access_token;
    window.plugin.glympse.expiry = Date.now() + 1000 * response.response.expires_in;

    window.plugin.glympse.updateStorage();
    window.plugin.glympse.showButton();
  };

  /**
   * Show Button
   *
   * Add the Glympse manager button to the map after successful authentication.
   * @return {void}
   */
  window.plugin.glympse.showButton = function() {
    if (window.useAndroidPanes()) {
      // android.addPane("plugin-glympse", "Glympse", "ic_action_paste");
      // addHook("paneChanged", window.plugin.glympse.onPaneChanged);
    } else {
      map.addControl(new window.plugin.glympse.controlBtn());
    }

    if (localStorage) {
      var storedSettings = localStorage.getItem("iitcGlympse");
      if (storedSettings && storedSettings.length) {
        var settings = JSON.parse(storedSettings);

        if (settings.layers) {
          $.each(settings.layers, function(layer, config) {
            window.plugin.glympse.create(layer, config);
          });
        }
      }
    }
  };

  /**
   * Dialog
   *
   * Launch the Glympse Manager dialog
   * @return {void}
   */
  window.plugin.glympse.dialog = function() {
    var html = "<div id=\"glympse-manager-tabs\">";

    html += "<ul>";
    html += "<li><a href=\"#glympse-summary\">Summary</a></li>";
    html += "<li><a href=\"#glympse-people\">People</a></li>";
    html += "</ul>";

    html += "<div id=\"glympse-summary\">";
    html += "<h3>Stats</h3>";
    html += "<p>Tracking " + Object.keys(window.plugin.glympse.people).length +
      " users in " + Object.keys(window.plugin.glympse.groups).length +
      " groups rendered in " + Object.keys(window.plugin.glympse.layers).length +
      " layers.</p>";
    html += "<table><thead><tr><th>Layer</th><th /></tr></thead><tbody>";
    $.each(window.plugin.glympse.layers, function(tag, layer) {
      html += "<tr><td class=\"glympse-locate layer\" data-id=\"" + tag + "\">" + tag + "</td><td>";
      html += "<button class=\"glympse-remove\" data-id=\"" + tag + "\">Remove</button> ";
      html += "<button class=\"glympse-update\" data-id=\"" + tag + "\">Reconfigure</button>";
      html += "</td></tr>";
    });
    html += "</tbody></table>";
    html += "<div><button id=\"glympse-add\">Add Layer</button></div>";
    html += "</div>";

    html += "<div id=\"glympse-people\">";
    html += "<table><thead><tr><th>User</th><th>Status</th><th>In Layers</th><th /></tr></thead><tbody>";
    $.each(window.plugin.glympse.people, function(identity, person) {
      var containers = $.map(person.invites, function(trackingObject, invite) {
        return (trackingObject.container) ? trackingObject.container.identity : invite;
      });

      html += "<tr><td class=\"glympse-locate person\" data-id=\"" + identity + "\">" + person.getName() + "</td><td>" +
        person.status + "</td><td>" + containers.join(", ") +
        "</td><td><button class=\"glympse-refresh\" data-id=\"" + identity + "\">Refresh</button></td></tr>";
    });
    html += "</tbody></table>";
    html += "</div>";

    html += "</div>";
    dialog({
      id: "glympse-manager",
      title: "Glympse Manager",
      html: html
    });

    $("#glympse-manager-tabs").tabs();
    $("#glympse-add").off("click");
    $("#glympse-add").on("click", function(event) {
      event.preventDefault();
      window.plugin.glympse.layerDialog();
    });

    $(".glympse-locate.person").off("click");
    $(".glympse-locate.person").on("click", function(event) {
      event.preventDefault();
      map.fitBounds(window.plugin.glympse.people[$(event.target).data("id")].layer.getBounds());
    });

    $(".glympse-locate.layer").off("click");
    $(".glympse-locate.layer").on("click", function(event) {
      event.preventDefault();
      map.fitBounds(window.plugin.glympse.layers[$(event.target).data("id")].layer.getBounds());
    });

    $(".glympse-remove").off("click");
    $(".glympse-remove").on("click", function(event) {
      window.plugin.glympse.destroy($(event.target).data("id"));
    });

    $(".glympse-refresh").off("click");
    $(".glympse-refresh").on("click", function(event) {
      window.plugin.glympse.people[$(event.target).data("id")].track();
    });

    $(".glympse-update").off("click");
    $(".glympse-update").on("click", function(event) {
      var id = $(event.target).data("id"),
          options = $.extend({}, window.plugin.glympse.layers[id].options);

      options.tag = id;

      window.plugin.glympse.layerDialog(options);
    });
  };

  /**
   * Layer Dialog
   *
   * Render dialog to add/update a layer.
   * @param  {Object} options Layer options
   * @return {void}
   */
  window.plugin.glympse.layerDialog = function(options) {
    if (options == null) {
      options = window.plugin.glympse.defaultOptions;
      options.tag = "";
    }

    var html = "<div>";

    html += "<fieldset><legend>Glympse Tag</legend>";
    html += "<input name=\"glympse-tag\" type=\"text\" value=\"" + options.tag + "\" />";
    html += "<p>Use either an individual invite (format of XXXX-XXXX) or a group tag (format of !tagname).</p>";
    html += "</fieldset>";

    html += "<fieldset><legend>Display Options</legend>";

    html += "<div><label for=\"icon-color\">Icon Background Color</label><input name=\"icon-color\" type=\"color\" value=\"" + options.icon.color + "\" /></div>";

    html += "<div><label for=\"show-name\">Show Glympse User Name</label><input name=\"show-name\" type=\"checkbox\" value=\"1\"" + ((options.showName) ? " checked=\"checked\"" : "") + " /></div>";

    html += "<div><label for=\"show-trails\">Show User Trail</label><input name=\"show-trails\" type=\"checkbox\" value=\"1\"" + ((options.showTrail) ? " checked=\"checked\"" : "") + " /></div>";

    html += "<div><label for=\"path-color\">Trail Color</label><input name=\"path-color\" type=\"color\" value=\"" + options.path.color + "\" /></div>";

    html += "<div><label for=\"shape\">Icon Shape:</label>";
    html += "<select name=\"shape\">";
    html += "<option value=\"round\"" + ((options.icon.shape === "round") ? " selected=\"selected\"" : "") + ">Circle</option>";
    html += "<option value=\"plain\"" + ((options.icon.shape === "plain") ? " selected=\"selected\"" : "") + ">Rectangle</option>";
    html += "<option value=\"bubble\"" + ((options.icon.shape === "bubble") ? " selected=\"selected\"" : "") + ">Speech Bubble</option>";
    html += "</select></div>";

    html += "<div><label for=\"icon\">Icon Image:</label>";
    html += "<select name=\"icon\">";
    html += "<option value=\"anchor\"" + ((options.icon.image === "anchor") ? " selected=\"selected\"" : "") + ">Anchor</option>";
    html += "<option value=\"bicycle\"" + ((options.icon.image === "bicycle") ? " selected=\"selected\"" : "") + ">Bicycle</option>";
    html += "<option value=\"bolt\"" + ((options.icon.image === "bolt") ? " selected=\"selected\"" : "") + ">Lightning Bolt</option>";
    html += "<option value=\"bomb\"" + ((options.icon.image === "bomb") ? " selected=\"selected\"" : "") + ">Bomb</option>";
    html += "<option value=\"building\"" + ((options.icon.image === "building") ? " selected=\"selected\"" : "") + ">Building</option>";
    html += "<option value=\"car\"" + ((options.icon.image === "car") ? " selected=\"selected\"" : "") + ">Car</option>";
    html += "<option value=\"eye\"" + ((options.icon.image === "eye") ? " selected=\"selected\"" : "") + ">Eye</option>";
    html += "<option value=\"female\"" + ((options.icon.image === "female") ? " selected=\"selected\"" : "") + ">Female</option>";
    html += "<option value=\"male\"" + ((options.icon.image === "male") ? " selected=\"selected\"" : "") + ">Male</option>";
    html += "<option value=\"map_pin\"" + ((options.icon.image === "map_pin") ? " selected=\"selected\"" : "") + ">Map Pin</option>";
    html += "<option value=\"shield\"" + ((options.icon.image === "shield") ? " selected=\"selected\"" : "") + ">Shielt</option>";
    html += "<option value=\"star\"" + ((options.icon.image === "star") ? " selected=\"selected\"" : "") + ">Star</option>";
    html += "<option value=\"thumb_tack\"" + ((options.icon.image === "thumb_tack") ? " selected=\"selected\"" : "") + ">Thumb Tack</option>";
    html += "<option value=\"truck\"" + ((options.icon.image === "truck") ? " selected=\"selected\"" : "") + ">truck</option>";
    html += "</select></div>";

    html += "<div><label for=\"override\">Override options for overlapping users</label><input name=\"override\" type=\"checkbox\" value=\"1\" /></div>";

    html += "</fieldset>";
    html += "</div>";

    dialog({
      closeCallback: window.plugin.glympse.handleSaveLayer,
      id: "glympse-layer-editor",
      title: (options.tag.length > 0) ? "Update " + options.tag : "Add Glympse Layer",
      html: html
    });
  };

  /**
   * Handle Save Layer
   *
   * Callback to layerDialog to process and save layer.
   * @return {void}
   */
  window.plugin.glympse.handleSaveLayer = function() {
    var invite = $("input[name=glympse-tag]").val(),
        showTrail = $("input[name=show-trails]").is(":checked"),
        showName = $("input[name=show-name]").is(":checked"),
        override = $("input[name=override]").is(":checked"),
        iconColor = $("input[name=icon-color]").val(),
        pathColor = $("input[name=path-color]").val(),
        iconShape = $("select[name=shape]").val(),
        icon = $("select[name=icon]").val(),
        newOptions = {
          showTrail: showTrail,
          showName: showName,
          override: override,
          icon: {
            color: iconColor,
            shape: iconShape,
            image: icon
          },
          path: {
            color: pathColor,
          }
        };

    if (window.plugin.glympse.layers[invite]) {
      window.plugin.glympse.layers[invite].setOptions(newOptions);
    } else {
      window.plugin.glympse.create(invite, newOptions);
    }
  };

  /**
   * Update Storage
   *
   * Refresh local storage data.
   * @param  {String} tag          Tag name to add/remove
   * @param  {Object|Bool} options Options to save, false to remove tag
   * @return {void}
   */
  window.plugin.glympse.updateStorage = function(tag, options) {
    if (!localStorage) {
      return;
    }

    if (options === null) {
      options = {};
    }

    var storedSettings = localStorage.getItem("iitcGlympse"),
        settings = (storedSettings && storedSettings.length) ? JSON.parse(storedSettings) : {};

    settings.token = window.plugin.glympse.token;
    settings.expiry = window.plugin.glympse.expiry;
    if (!settings.layers) {
      settings.layers = {};
    }

    if (settings.layers[tag] && options == false) {
      delete settings.layers[tag];
    } else if (tag) {
      settings.layers[tag] = options;
    }

    localStorage.setItem("iitcGlympse", JSON.stringify(settings));
  };

  /**
   * Parse Options
   *
   * Create a full option set with classes to push into Leaflet.
   * @param  {Object} options Slimmed options.
   * @return {Object}         Full options
   */
  window.plugin.glympse.parseOptions = function(options) {
    newOptions = $.extend({}, window.plugin.glympse.defaultOptions, options);

    newOptions.marker.icon = L.divIcon({
      className: "glympse-member " + newOptions.icon.shape + " " + newOptions.icon.image,
      iconSize: [16, 24],
      iconAnchor: [16, (newOptions.icon.shape === "bubble") ? 48 : 32],
      popupAnchor: [0, -31]
    });

    return newOptions;
  };

  /**
   * Create
   *
   * Register a new Glympse Layer for the Map.
   * @param  {String} tag     Glympse tag name
   * @param  {Object} options Display options
   * @return {GlympseLayer}
   */
  window.plugin.glympse.create = function(tag, options) {
    if (options === null) {
      options = {};
    }

    // If tag doesn't match an invite or group tag, make it a group
    if (!tag.match(/(^!|^[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}$)/)) {
      tag = "!" + tag;
    }

    var layer = new GlympseLayer(tag, options);
    window.plugin.glympse.updateStorage(layer.getName(), options);

    return layer;
  };

  /**
   * Destroy
   *
   * Delete a layer.
   * @param  {String} tag Tag name to destroy.
   * @return {void}
   */
  window.plugin.glympse.destroy = function(tag) {
    if (window.plugin.glympse.layers[tag]) {
      window.plugin.glympse.updateStorage(window.plugin.glympse.layers[tag].getName(), false);
      window.plugin.glympse.layers[tag].remove();
      delete window.plugin.glympse.layers[tag];
    } else {
      console.warn("Could not delete", tag, " because it does not exist");
    }
  };

  /**
   * Time Since
   *
   * Calculate relative time difference.
   * @param  {Date|Number} toTime   To Time
   * @param  {Date|Number} fromTime From Time (default is now)
   * @return {String}               Relative time description (e.g. 2 minutes ago)
   */
  window.plugin.glympse.timeSince = function (toTime, fromTime) {
    toTime = new Date(toTime).valueOf();
    if (fromTime == null) {
      fromTime = Date.now();
    } else {
      fromTime = new Date(fromTime).valueOf();
    }

    var isPast = (fromTime > toTime),
        diff = Math.round(Math.abs(fromTime - toTime)/1000),
        timeSince = "second";

    // Switch to minutes if more than 2 minutes
    if (diff > 120) {
      diff /= 60;
      timeSince = "minutes";
    }

    // Switch to hours if more than 1.5 hours
    if (diff > 90) {
      diff /= 60;
      timeSince = "hour";
    }

    diff = "about " + Math.floor(diff) + " " + timeSince + "(s)";

    return (isPast) ? diff + " ago" : "in " + diff;
  };

  var setup = function () {
    window.addHook('iitcLoaded', window.plugin.glympse.init);

    $(document).on("click", ".glympse-popup a", function (event) {
      map.setView($(event.target).attr("data-target").split(","), 18);
      map.closePopup();
    });
  };

  setup.info = null;

  if(!window.bootPlugins) {
    window.bootPlugins = [];
  }

  window.bootPlugins.push(setup);

  // if IITC has already booted, immediately run the 'setup' function
  if(window.iitcLoaded) {
    setup();
  }
}

// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) {
  info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
  };
}
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);

/*jshint multistr: true */
var style = document.createElement('style');
style.appendChild(document.createTextNode(
  ".glympse-member { \
    background-color: white;\
    background-position: 50% 2px;\
    background-repeat: no-repeat;\
    background-size: 24px 28px;\
    border: 4px solid blue;\
    padding: 4px;\
    position: relative;\
  }\
  .glympse-member.fresh {\
    border-color: green;\
  }\
  .glympse-member.stale {\
    border-color: orange;\
  }\
  .glympse-member.old {\
    border-color: red;\
  }\
  .glympse-member.expired {\
    border-color: grey;\
  }\
  .glympse-member.selected {\
    border-color: yellow;\
  }\
  .glympse-member.selected > div {\
    box-shadow: 1px 1px 2px #FF00FF;\
  }\
  .glympse-member.round { \
    border-radius: 50%;\
  }\
  .glympse-member.bubble:after{\
  }\
  .glympse-member.bubble:after{\
    content: \"\";\
    position: absolute;\
    bottom: -12px;\
    left: 6px;\
    border-width: 12px 6px 0;\
    border-style: solid;\
    border-color: transparent;\
    border-top-color: inherit;\
    border-bottom-color: inherit;\
    display: block;\
    width: 0;\
  }\
  .glympse-member > div {\
    background-color: #FFFFFF;\
    border-color: inherit;\
    border-radius: 3px;\
    border-width: 3px;\
    border-style: solid;\
    color: #333333;\
    text-align: center;\
    text-shadow: 1px 1px 1px #AAAAAA;\
    text-overflow: ellipsis;\
    overflow: hidden;\
    white-space: nowrap;\
  }\
  .glympse-member.above > div {\
    margin: -31px -32px 0;\
  }\
  .glympse-member.male {\
    background-image: url(" + window.plugin.glympse.icons.male + ");\
  }\
  .glympse-member.female {\
    background-image: url(" + window.plugin.glympse.icons.female + ");\
  }\
  .glympse-member.anchor {\
    background-image: url(" + window.plugin.glympse.icons.anchor + ");\
  }\
  .glympse-member.bicycle {\
    background-image: url(" + window.plugin.glympse.icons.bicycle + ");\
  }\
  .glympse-member.bolt {\
    background-image: url(" + window.plugin.glympse.icons.bolt + ");\
  }\
  .glympse-member.bomb {\
    background-image: url(" + window.plugin.glympse.icons.bomb + ");\
  }\
  .glympse-member.building {\
    background-image: url(" + window.plugin.glympse.icons.building + ");\
  }\
  .glympse-member.car {\
    background-image: url(" + window.plugin.glympse.icons.car + ");\
  }\
  .glympse-member.eye {\
    background-image: url(" + window.plugin.glympse.icons.eye + ");\
  }\
  .glympse-member.map_pin {\
    background-image: url(" + window.plugin.glympse.icons.map_pin + ");\
  }\
  .glympse-member.shield {\
    background-image: url(" + window.plugin.glympse.icons.shield + ");\
  }\
  .glympse-member.star {\
    background-image: url(" + window.plugin.glympse.icons.star + ");\
  }\
  .glympse-member.thumb_tack {\
    background-image: url(" + window.plugin.glympse.icons.thumb_tack + ");\
  }\
  .glympse-member.truck {\
    background-image: url(" + window.plugin.glympse.icons.truck + ");\
  }\
  .glympse-popup {\
    max-width: 200px;\
  }\
  .glympse-popup h2 {\
    text-align: center;\
  }\
  .glympse-popup img {\
    width: 100px;\
    height: 100px;\
    display:block;\
    margin: 0 auto;\
    border: 1px solid #FFCE00;\
    padding: 3px;\
  }\
  .glympse-popup a {\
    color: #FFFFFF;\
    text-decoration: underline;\
  }\
  .glympse-popup .text-center {\
    text-align: center;\
  }\
  .leaflet-bar.glympse a {\
    background-repeat: no-repeat;\
    background-image: url(" + window.plugin.glympse.icons.glympse + ");\
  }\
  .leaflet-bar.glympse:hover a {\
    background-image: url(" + window.plugin.glympse.icons.glympseHover + ");\
  }\
  .leaflet-retina .leaflet-bar.glympse a {\
    background-size: 20px 20px;\
    background-image: url(" + window.plugin.glympse.icons.glympseRetinaHover + ");\
  }\
  .leaflet-retina .leaflet-bar.glympse:hover a {\
    background-image: url(" + window.plugin.glympse.icons.glympseRetina + ");\
  }\
  #dialog-glympse-manager button {\
    cursor: pointer;\
  }"
));
(document.body || document.head || document.documentElement).appendChild(style);
