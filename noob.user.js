// // ==UserScript==
// @id             iitc-plugin-noob-patrol@jonatkins
// @name           IITC plugin: Noob Patrol
// @version        1.1.20160716
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @description    Noob Patrol
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @grant          none

// ==/UserScript==


function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};

//PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
//(leaving them in place might break the 'About IITC' page or break update checks)

//END PLUGIN AUTHORS NOTE



// PLUGIN START ////////////////////////////////////////////////////////


// use own namespace for plugin
    window.plugin.noobPatrol = function() {};
    window.plugin.noobPatrol.setup=function()
    {
        window.plugin.noobPatrol.loadData();
        window.plugin.noobPatrol.loadOptions();
        $('#toolbox').append('<a onclick="window.plugin.noobPatrol.showOptionsDialog();return false;">Noob Patrol Options</a>');
        if(window.iitcLoaded===true)
            window.plugin.noobPatrol.onIITCLoaded();
        else addHook('iitcLoaded', window.plugin.noobPatrol.onIITCLoaded);

    };

// Chat Handling ///////////////////////////////////////////////////////
    window.plugin.noobPatrol.targetTime=-1;
    window.plugin.noobPatrol.oldestTime=-1;
    window.plugin.noobPatrol.lastScanResults=new Array();
    window.plugin.noobPatrol.scanChat=function()
    {
        //reload data incase there are multiple sessions.
        window.plugin.noobPatrol.loadData();
        var daysToScan=window.plugin.noobPatrol.noobPatrolOptions.daysToScan;
        var millisToScan=daysToScan*24*60*60*1000;
        var currentTime=new Date().getTime();
        window.plugin.noobPatrol.targetTime=currentTime-millisToScan;
        window.plugin.noobPatrol.oldestTime=-1;
        window.plugin.noobPatrol.lastScanResults=new Array();
        console.log("Noob Patrol: scanChat: daysToScan: "+daysToScan);
        window.plugin.noobPatrol.requestFactionChat();

    }

    window.plugin.noobPatrol.onIITCLoaded=function()
    {
        if(window.plugin.noobPatrol.noobPatrolOptions.scanOnStartUp!==undefined&&window.plugin.noobPatrol.noobPatrolOptions.scanOnStartUp===true)
            window.plugin.noobPatrol.scanChat();
    }

    window.plugin.noobPatrol.requestFactionChat=function()
    {
        console.log("Noob Patrol: requestFactionChat");
        var chatMaxLatLng=window.plugin.noobPatrol.noobPatrolOptions.maxChatLatLng;
        var chatMinLatLng=window.plugin.noobPatrol.noobPatrolOptions.minChatLatLng;

        var data = {
            minLatE6: Math.round(chatMinLatLng.lat*1E6),
            minLngE6: Math.round(chatMinLatLng.lng*1E6),
            maxLatE6: Math.round(chatMaxLatLng.lat*1E6),
            maxLngE6: Math.round(chatMaxLatLng.lng*1E6),
            minTimestampMs: window.plugin.noobPatrol.targetTime,
            maxTimestampMs: window.plugin.noobPatrol.oldestTime,
            tab: 'faction',
        }
        var r = window.postAjax(
            'getPlexts',
            data,
            function(data, textStatus, jqXHR) {window.plugin.noobPatrol.requestFactionChatCallback(data); },
            function() { console.log("Noob Patrol: Custom Faction Chat Request Failed - latE6="+latE6+", lngE6="+lngE6+", created="+created); }
        );

    };

    window.plugin.noobPatrol.requestFactionChatCallback=function(data)
    {
        var urlToDetect=window.plugin.noobPatrol.noobPatrolOptions.urlToDetect;
        for(var chatidx=0;chatidx<data.result.length;chatidx++)
        {
            var chatrow=data.result[chatidx];

            if(window.plugin.noobPatrol.oldestTime<0||window.plugin.noobPatrol.oldestTime>chatrow[1])
                window.plugin.noobPatrol.oldestTime=chatrow[1]-1;

            var type=chatrow[2].plext.plextType;
            if(type==="PLAYER_GENERATED")
            {

                var guid=chatrow[0];
                var timestamp=chatrow[1];
                var team=null;
                var senderName=null;
                var playerName=null;
                var chattext=null;
                var invite=false;
                var eventType=null;
                var toAgents=new Array();
                for(var i=0;i<chatrow[2].plext.markup.length;i++)
                {
                    var markup=chatrow[2].plext.markup[i];
                    if(markup[0]==="SENDER")
                    {
                        team=markup[1].team;
                        senderName=markup[1].plain.replace(":","").trim();
                    }
                    if(markup[0]==="AT_PLAYER")
                    {
                        var toagent=markup[1].plain.replace("@","").trim();
                        toAgents.push(toagent);
                    }
                    if(markup[0]==="PLAYER")
                    {
                        playerName=markup[1].plain.trim();
                    }
                    if(markup[0]==="TEXT")
                    {
                        var text=markup[1].plain;
                        var tmptext=markup[1].plain.trim().toLowerCase();
                        if(tmptext.indexOf(urlToDetect.trim().toLowerCase())>=0)
                            invite=true;
                        if(text.indexOf("has completed training")>=0)
                            eventType="completedTraining";
                        if(text.indexOf("created their first Link")>=0)
                            eventType="firstLink";
                        if(text.indexOf("captured their first Portal")>=0)
                            eventType="firstCapture";
                        if(text.indexOf("created their first Control Field")>=0)
                            eventType="firstField";

                    }
                }

                if(invite)
                {
                    for(var j=0;j<toAgents.length;j++)
                    {
                        var toAgent=toAgents[j];
                        if(window.plugin.noobPatrol.isAlreadyInvited(toAgent)===false)
                        {
                            window.plugin.noobPatrol.setInviteBy(toAgent,senderName,timestamp);
                        }
                    }
                }

                if(eventType!==null)
                {
                    //sometimes it comes up as player and sometimes sender.
                    if(playerName===null) playerName=senderName;
                    window.plugin.noobPatrol.setTimestamp(playerName,eventType,timestamp);
                    if(window.plugin.noobPatrol.lastScanResults.indexOf(playerName)<0)
                        window.plugin.noobPatrol.lastScanResults.push(playerName);
                }

            }
        }

        //if there is still data and we haven't hit our target time, ask for more.
        if(data.result.length>0 && window.plugin.noobPatrol.oldestTime>window.plugin.noobPatrol.targetTime)
            window.plugin.noobPatrol.requestFactionChat();
        else window.plugin.noobPatrol.showScanResults();
    };


// End Chat Handling ///////////////////////////////////////////////////
// Invite Handling /////////////////////////////////////////////////////
    window.plugin.noobPatrol.showScanResults=function()
    {
        var lastScanResults=window.plugin.noobPatrol.lastScanResults;
        window.plugin.noobPatrol.selectedAgents=new Array();
        var checkbox='<input type="checkbox" id="noobPatrol-agent" onclick="window.plugin.noobPatrol.allSelect(this);"/>';
        html="<table style=\"width:650px;\" class='sortable'><thead><tr><th class=\"sorttable_nosort\"><b>" + checkbox + " Send</b></th><th><b>Agent Name</b></th><th><b>Invited At</b></th><th><b>Invited By</b></th><th><b>Training</b></th><th><b>First Capture</b></th><th><b>First Link</b></th><th><b>First Field</b></th></tr></thead><tbody>";
        for(var i=0;i<lastScanResults.length;i++)
        {
            var agentName=lastScanResults[i];
            var agentData=window.plugin.noobPatrol.noobPatrolData[agentName];
            if(agentData!==undefined)
            {
                var completedTraining=agentData.completedTraining;
                var firstCapture=agentData.firstCapture;
                var firstLink=agentData.firstLink;
                var firstField=agentData.firstField;
                var communityInvite=agentData.communityInvite;
                var communityInviteBy=agentData.communityInviteBy;

                if(completedTraining===undefined) completedTraining="";
                else completedTraining=window.unixTimeToDateTimeString(completedTraining,false).substring(0,10);

                if(firstCapture===undefined) firstCapture="";
                else firstCapture=window.unixTimeToDateTimeString(firstCapture,false).substring(0,10);

                if(firstLink===undefined) firstLink="";
                else firstLink=window.unixTimeToDateTimeString(firstLink,false).substring(0,10);

                if(firstField===undefined) firstField="";
                else firstField=window.unixTimeToDateTimeString(firstField,false).substring(0,10);

                if(communityInvite===undefined) communityInvite="";
                else communityInvite=window.unixTimeToDateTimeString(communityInvite,false).substring(0,10);

                if(communityInviteBy===undefined) communityInviteBy="";

                var checked=""
                if(communityInvite==="")
                {
                    checked="checked";
                    window.plugin.noobPatrol.agentSelection(agentName,false);
                }

                var checkbox='<input type="checkbox" id="noobPatrol-agent-'+agentName+'" value="yes" onclick="window.plugin.noobPatrol.agentSelection(\''+agentName+'\',this.checked);"/>';
                html+="<tr><td>"+checkbox+"</td><td>"+agentName+"</td><td>"+communityInvite+"</td><td>"+communityInviteBy+"</td><td>"+completedTraining+"</td><td>"+firstCapture+"</td><td>"+firstLink+"</td><td>"+firstField+"</td></tr>";
            }
        }
        html+="</tbody></table>";
        html+="<br/><div style=\"text-align: center;\">";
        html+="<span id=\"noob-patrol-message-preview\"><textarea id=\"noob-patrol-message\" rows=\"5\" cols=\"50\" maxlength=\"200\" onblur=\"window.plugin.noobPatrol.updatePreview();\">"+window.plugin.noobPatrol.noobPatrolOptions.messageTemplate+"</textarea>"
        html+='<br/><input type="number" style="text-align: right" id="sendTime" value="0" /> Seconds<br/>Time between the post<br/>'
        html+='<br/>Send as <input type="radio" id="group" name="type" value="0" checked="checked" onclick="window.plugin.noobPatrol.typ(this)"><label style="color:#FFCE00;cursor: pointer" for="group"> Group</label> or <input type="radio" id="single" name="type" value="1" onclick="window.plugin.noobPatrol.typ(this)"> <label style="color:#FFCE00;cursor: pointer" for="single"> Single</label> Message'
        html+="<br/><br/><div id=\"noob-patrol-message-breakdown\" style=\"width:370px;display:inline-block;\"></div></span>";
        html+="<br/><br><span><h3>Send message to noobs?</h3></span>";
        html+= "<br/><button type=\"button\" onclick=\"window.plugin.noobPatrol.sendAllMessages();\" style=\"height:100px;width:250px;cursor:pointer\" \"class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only\" role=\"button\"><span class=\"ui-button-text\"><h1>FUCK YEAH!!</h1></span></button>";
        html+="</div>";
        window.dialog({title:'Noob Patrol Scan Results',modal: false, width:'auto',id: 'noob-patrol-scan-results',html:html, height:'auto'});
        window.plugin.noobPatrol.updatePreview();
        window.plugin.noobPatrol.addSorttable();

    };
    window.plugin.noobPatrol.selectedAgents=new Array();
    window.plugin.noobPatrol.agentSelection=function(agentName,selected)
    {
        var agentIdx=window.plugin.noobPatrol.selectedAgents.indexOf(agentName);
        if(selected===true && agentIdx<0) window.plugin.noobPatrol.selectedAgents.push(agentName);
        if(selected===false && agentIdx>=0) window.plugin.noobPatrol.selectedAgents.splice(agentIdx,1);
        window.plugin.noobPatrol.updatePreview();

    };
    window.plugin.noobPatrol.addSorttable = function(){

        if(document.getElementById("sortable_is_on") != null){
            (elem = document.getElementById("sortable_is_on")).parentNode.removeChild(elem);
        }
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.id = 'sortable_is_on';
            script.src = 'https://www.kryogenix.org/code/browser/sorttable/sorttable.js';
            document.getElementsByTagName('head')[0].appendChild(script);
    }

    window.plugin.noobPatrol.allSelect = function(select){
        //select.parent.parent.parent.nextChild.getElementsByTagName("input")
        var tbody = select.parentElement.parentElement.parentElement.parentElement.nextSibling;
        var input = tbody.getElementsByTagName("input");
        for (var i = 0; i < input.length; i++) {
            input[i].checked = select.checked;
            var agentName = input[i].getAttribute("id").replace("noobPatrol-agent-","");
            window.plugin.noobPatrol.agentSelection(agentName,select.checked);

        }
        console.log(input.length)
    }

    window.plugin.noobPatrol.typ = function(type){
        // if 0 = group | if 1 = single
        window.plugin.noobPatrol.updatePreview();
    }

    window.plugin.noobPatrol.getTyp = function(){
        if(document.getElementById("noob-patrol-message-preview") != null){
            var typ = document.getElementById("single")
            //var typGroup = typ[0];
            //var typSingle = typ[1];
            return (typ.checked ? 1 : 10);
        }
        return 10;
    }

    window.plugin.noobPatrol.updatePreview=function()
    {
        //var msg=window.plugin.noobPatrol.constructMessage();
        //var html="<span id=\"noob-patrol-message-preview\"><textarea id=\"noob-patrol-message\" rows=\"5\" cols=\"50\">"+msg+"</textarea></span>"
        //$('#noob-patrol-message-preview').replaceWith(html);
        var html="<div id=\"noob-patrol-message-breakdown\" style=\"width:370px;display:inline-block;\">";
        var msgs=window.plugin.noobPatrol.constructMessages();
        for(var i=0;i<msgs.length;i++)
        {
            var msgCount=i+1;
            html+="<span><b>Message "+msgCount+":</b></span>";
            html+="<br/><span>"+msgs[i]+"</span>";
            if(msgCount<msgs.length)
                html+="<br/><br/>";

        }
        html+="</div>";
        $('#noob-patrol-message-breakdown').replaceWith(html);
    };

    window.plugin.noobPatrol.constructMessages=function()
    {
        var msg="";
        var msgTemplate=$.trim($('#noob-patrol-message').val());
        var agents=window.plugin.noobPatrol.selectedAgents;
        var templateLength=msgTemplate.length;
        var output=new Array();
        if(agents.length===0) return msg;
        var curAgentCount=0;
        for(var i=0;i<agents.length;i++)
        {
            //if adding this agent would push the message length over 250 (slightly smaller than the actual limit of 255)
            if((msg.length+agents[i].length+2+templateLength)>250|curAgentCount>=window.plugin.noobPatrol.getTyp())
            {
                output.push(msg+msgTemplate);
                msg="";
                curAgentCount=0;
            }
            msg+="@"+agents[i]+" ";
            curAgentCount++;
        }
        output.push(msg+msgTemplate);
        //msg+=window.plugin.noobPatrol.noobPatrolOptions.messageTemplate;
        return output;
    };
    window.plugin.noobPatrol.messagesToSend=new Array();
    window.plugin.noobPatrol.sendAllMessages=function()
    {
        window.plugin.noobPatrol.messagesToSend=window.plugin.noobPatrol.constructMessages();
        window.plugin.noobPatrol.sendNextMessage(document.getElementById("sendTime").value);

    }
    window.plugin.noobPatrol.sendNextMessage=function(time) {
        //var msg=$.trim($('#noob-patrol-message').val());
        if (window.plugin.noobPatrol.messagesToSend.length === 0) return;
        var msg = window.plugin.noobPatrol.messagesToSend[0];
        var chatMaxLatLng = window.plugin.noobPatrol.noobPatrolOptions.maxChatLatLng;
        var chatMinLatLng = window.plugin.noobPatrol.noobPatrolOptions.minChatLatLng;

        var latE6 = Math.round(((chatMinLatLng.lat + chatMaxLatLng.lat) / 2) * 1E6);
        var lngE6 = Math.round(((chatMinLatLng.lng + chatMaxLatLng.lng) / 2) * 1E6);


        //var latlng = map.getCenter();

        var errMsg = 'Your message could not be delivered. You can copy&' +
            'paste it here and try again if you want:\n\n' + msg;
        var data = {
            message: msg,
            //latE6: Math.round(latlng.lat*1E6),
            //lngE6: Math.round(latlng.lng*1E6),
            latE6: latE6,
            lngE6: lngE6,
            tab: 'faction'
        };
        /*alert(msg);
         window.plugin.noobPatrol.messagesToSend.splice(0,1);
         if(window.plugin.noobPatrol.messagesToSend.length>0)
         window.plugin.noobPatrol.sendNextMessage();
         else
         startRefreshTimeout(0.1*1000); //only chat uses the refresh timer stuff, so a perfect way of forcing an early refresh after a send message
         */
        //console.log(msg)
        //window.plugin.noobPatrol.messagesToSend.splice(0, 1);
        ////var time = document.getElementById("sendTime").value
        //console.log(time);
        //if (window.plugin.noobPatrol.messagesToSend.length > 0){
        //    self.setTimeout(function(){
        //        window.plugin.noobPatrol.sendNextMessage(time);
        //    },time*1000);
        //
        //} else {
        //    //startRefreshTimeout(0.1*1000); //only chat uses the refresh timer stuff, so a perfect way of forcing an early refresh after a send message
        //alert("Send messege is Finish")
        //    console.log('end')
        //}

        window.postAjax('sendPlext', data,
            function(response) {
                if(response.error) alert(errMsg);
                window.plugin.noobPatrol.messagesToSend.splice(0,1);
                if(window.plugin.noobPatrol.messagesToSend.length>0)
                    self.setTimeout(function(){
                        console.log(msg)
                        window.plugin.noobPatrol.sendNextMessage(time);
                    },time*1000);
                else
                    alert("Send messege is Finish")
                    startRefreshTimeout(0.1*1000); //only chat uses the refresh timer stuff, so a perfect way of forcing an early refresh after a send message
            },
            function() {
                alert(errMsg);
            }
        );

    };
// Invite Handling ///////////////////////////////////////////////////////

// Data Handling ///////////////////////////////////////////////////////

    window.plugin.noobPatrol.loadData=function()
    {
        var optionsStr=localStorage['noob-patrol-data'];
        if(optionsStr!==undefined && optionsStr!=="")
            window.plugin.noobPatrol.noobPatrolData=JSON.parse(optionsStr);
        else
            window.plugin.noobPatrol.noobPatrolData={};
        /*
         Structure:
         {
         "crackedMagnet":{
         "completedTraining":389408904234,
         "firstCapture":5435435345,
         "firstLink":5435345345,
         "firstField":5435435345,
         "communityInvite":8590348543,
         "communityInviteBy":"invitingAgent"
         },
         "anotherAgent":{
         "completedTraining":389408904234,
         "firstCapture":5435435345,
         "firstLink":5435345345,
         "firstField":5435435345,
         "communityInvite":8590348543,
         "communityInviteBy":"invitingAgent"
         }
         }
         */
    };
    window.plugin.noobPatrol.saveData=function()
    {
        var data=window.plugin.noobPatrol.noobPatrolData;
        localStorage['noob-patrol-data']=JSON.stringify(data);
    };

    window.plugin.noobPatrol.setTimestamp=function(agentName,eventType,timestamp)
    {
        var agentData=window.plugin.noobPatrol.noobPatrolData[agentName];
        if(agentData===undefined)
            agentData={};
        agentData[eventType]=timestamp;
        window.plugin.noobPatrol.noobPatrolData[agentName]=agentData;
        window.plugin.noobPatrol.saveData();
    };

    window.plugin.noobPatrol.setInviteBy=function(agentName,invitedBy,timestamp)
    {
        var agentData=window.plugin.noobPatrol.noobPatrolData[agentName];
        if(agentData===undefined)
            agentData={};
        agentData["communityInvite"]=timestamp;
        agentData["communityInviteBy"]=invitedBy;
        window.plugin.noobPatrol.noobPatrolData[agentName]=agentData;
        window.plugin.noobPatrol.saveData();
    };

    window.plugin.noobPatrol.isAlreadyInvited=function(agentName)
    {
        var agentData=window.plugin.noobPatrol.noobPatrolData[agentName];
        if(agentData===undefined) return false;
        if(agentData.communityInvite!==undefined) return true;
        return false;
    };

// End Data Handling ///////////////////////////////////////////////////    

// Options /////////////////////////////////////////////////////////////

    window.plugin.noobPatrol.loadOptions=function()
    {
        var optionsStr=localStorage['noob-patrol-options'];
        if(optionsStr!==undefined && optionsStr!=="")
            window.plugin.noobPatrol.noobPatrolOptions=JSON.parse(optionsStr);
        else
            window.plugin.noobPatrol.noobPatrolOptions={};


        if(window.plugin.noobPatrol.noobPatrolOptions.daysToScan===undefined)
            window.plugin.noobPatrol.noobPatrolOptions.daysToScan=14;

        if(window.plugin.noobPatrol.noobPatrolOptions.maxChatLatLng===undefined)
            window.plugin.noobPatrol.setTargetBoundry();

        if(window.plugin.noobPatrol.noobPatrolOptions.messageTemplate===undefined)
            window.plugin.noobPatrol.noobPatrolOptions.messageTemplate="[enter your invite message here]";

        if(window.plugin.noobPatrol.noobPatrolOptions.scanOnStartUp===undefined)
            window.plugin.noobPatrol.noobPatrolOptions.scanOnStartUp=false;

        if(window.plugin.noobPatrol.noobPatrolOptions.urlToDetect===undefined)
            window.plugin.noobPatrol.noobPatrolOptions.urlToDetect="example.com/community";




    };


    window.plugin.noobPatrol.saveOptions=function()
    {
        var options=window.plugin.noobPatrol.noobPatrolOptions;
        localStorage['noob-patrol-options']=JSON.stringify(options);
    };



    window.plugin.noobPatrol.getOptionCheckboxTag=function(option,extraClickJS)
    {
        if(extraClickJS===undefined) extraClickJS="";
        var checked="";
        if(window.plugin.noobPatrol.noobPatrolOptions[option]===true) checked="checked";
        return '<input type="checkbox" id="noobPatrol'+option+'Option" value="yes" '+checked+' onclick="window.plugin.noobPatrol.updateOptionCheckbox(\''+option+'\',this.checked);'+extraClickJS+'"/>';

    };

    window.plugin.noobPatrol.getOptionNumericTag=function(option,width,extraClickJS)
    {
        if(extraClickJS===undefined) extraClickJS="";
        if(width===undefined) width=30;
        var value=window.plugin.noobPatrol.noobPatrolOptions[option];
        return '<input type="text" id="noobPatrol'+option+'Option"  style="width:'+width+'px" value="'+value+'" onblur="window.plugin.noobPatrol.updateOptionNumeric(\''+option+'\',this.value);'+extraClickJS+'"/>';
    };

    window.plugin.noobPatrol.getOptionTextTag=function(option,width,extraClickJS)
    {
        if(extraClickJS===undefined) extraClickJS="";
        if(width===undefined) width=30;
        var value=window.plugin.noobPatrol.noobPatrolOptions[option];
        return '<input type="text" id="noobPatrol'+option+'Option"  style="width:'+width+'px" value="'+value+'" onblur="window.plugin.noobPatrol.updateOptionText(\''+option+'\',this.value);'+extraClickJS+'"/>';
    };

    window.plugin.noobPatrol.getOptionTextAreaTag=function(option,width,height,extraClickJS)
    {
        if(extraClickJS===undefined) extraClickJS="";
        if(width===undefined) width=30;
        var value=window.plugin.noobPatrol.noobPatrolOptions[option];
        return '<textarea type="text" id="noobPatrol'+option+'Option"  style="width:'+width+'px;height:'+height+'px;" onblur="window.plugin.noobPatrol.updateOptionText(\''+option+'\',this.value);'+extraClickJS+'">'+value+'</textarea>';
    };

    window.plugin.noobPatrol.updateOptionCheckbox=function(option,checked)
    {
        window.plugin.noobPatrol.noobPatrolOptions[option]=checked;
        window.plugin.noobPatrol.saveOptions();
    };

    window.plugin.noobPatrol.updateOptionNumeric=function(option,value)
    {
        var numValue=parseFloat(value);
        window.plugin.noobPatrol.noobPatrolOptions[option]=numValue;
        window.plugin.noobPatrol.saveOptions();
    };

    window.plugin.noobPatrol.updateOptionText=function(option,value)
    {
        window.plugin.noobPatrol.noobPatrolOptions[option]=value;
        window.plugin.noobPatrol.saveOptions();
    };
    window.plugin.noobPatrol.setTargetBoundry=function()
    {
        var bounds=clampLatLngBounds(map.getBounds());
        window.plugin.noobPatrol.noobPatrolOptions["maxChatLatLng"]=bounds.getNorthEast();
        window.plugin.noobPatrol.noobPatrolOptions["minChatLatLng"]=bounds.getSouthWest();
        window.plugin.noobPatrol.saveOptions();

    };
    window.plugin.noobPatrol.showOptionsDialog=function()
    {
        console.log("Noob Patrol: showOptionsDialog");
        var html="";
        html+="<br/><span>Noob Patrol scans faction chat for new agents.</span>";
        html+="<br/><br/><span><a href=\"javascript:window.plugin.noobPatrol.setTargetBoundry();\">Reset chat scan boundry to current view</a></span>";
        html+="<br/><br/><span>Message to send</span>";
        html+="<br/><span>"+window.plugin.noobPatrol.getOptionTextAreaTag("messageTemplate",350,200)+"</span>";
        html+="<br/><br/><span># Days to scan</span>";
        html+="<br/><span>"+window.plugin.noobPatrol.getOptionNumericTag("daysToScan",30)+"</span>";
        html+="<br/><br/><span>URL to detect (incase other agents invite)</span>";
        html+="<br/><span>"+window.plugin.noobPatrol.getOptionTextTag("urlToDetect",200)+"</span>";
        html+="<br/><br/><span>"+window.plugin.noobPatrol.getOptionCheckboxTag("scanOnStartUp")+" Scan on IITC Start</span>";
        html+="<br/><br/><span><a href=\"javascript:window.plugin.noobPatrol.scanChat();\">Scan Now</a></span>";

        window.dialog({title:'Noob Patrol Options',modal: false, width:'auto',id: 'noob-patrol-options',html:html, height:'auto'});

    };


// Options End /////////////////////////////////////////////////////////

    var setup =  window.plugin.noobPatrol.setup;


// PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context


var script = document.createElement('script');
var info = {};




if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);